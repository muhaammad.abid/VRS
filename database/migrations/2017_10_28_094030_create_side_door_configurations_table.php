<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSideDoorConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('side_door_configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('abbreviation');
//            $table->string('abbreviation');
            $table->integer('year_idFk')->unsigned();
            $table->timestamps();
        });

        Schema::table('side_door_configurations', function($table) {
            $table->foreign('year_idFk')->references('id')->on('years')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('side_door_configurations');
    }
}
