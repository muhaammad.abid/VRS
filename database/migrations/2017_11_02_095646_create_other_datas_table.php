<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parking');
            $table->string('painted');
            $table->string('trim');
            $table->string('type');
            $table->string('other');
            $table->integer('year_idFk')->unsigned();
            $table->timestamps();
        });

        Schema::table('other_datas', function($table) {
            $table->foreign('year_idFk')->references('id')->on('years')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_datas');
    }
}
