<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rows', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parking')->nullable();
            $table->string('painted')->nullable();
            $table->string('trim')->nullable();
            $table->string('type')->nullable();
            $table->string('other')->nullable();
            $table->string('rear_door_config')->nullable();
            $table->string('rdc_abbreviation')->nullable();
            $table->string('side_door_config')->nullable();
            $table->string('sdc_abbreviation')->nullable();
            $table->string('roof_config')->nullable();
            $table->string('rc_abbreviation')->nullable();
            $table->string('wheel_base')->nullable();
            $table->string('wb_abbreviation')->nullable();
            $table->string('year')->nullable();
            $table->string('model')->nullable();
            $table->string('make')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rows');
    }
}
