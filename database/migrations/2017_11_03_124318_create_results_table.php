<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand')->nullable();
            $table->string('variant')->nullable();
            $table->string('end_use')->nullable();
            $table->string('material')->nullable();
            $table->string('grade')->nullable();
            $table->string('magnetic')->default('off');
            $table->string('microskin')->default('off');
            $table->string('stone_guard')->default('off');
            $table->string('painted')->default('off');
            $table->integer('make')->nullable();
            $table->integer('model')->nullable();
            $table->integer('year')->nullable();
            $table->integer('wheel_base')->nullable();
            $table->integer('roof_config')->nullable();
            $table->integer('side_door_config')->nullable();
            $table->integer('rear_door')->nullable();
            $table->string('parking')->nullable();
            $table->text('name')->nullable();
            $table->text('shortname')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
