<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'first_name' => 'Admin',
                'last_name' => 'Kashif',
                'email'     => 'admin@admin.com',
                'password'  => bcrypt('admin'),
                'user_role_idFk' => 1,
                'image' => 'default.jpg'
            ]
        ]);

        DB::table('battenberg_variants')->insert([
            [
                'name' => 'Full Battenburg',
                'abbreviation' => 'FB'
            ],[
                'name' => '3-Quarter Battenburg',
                'abbreviation' => '3Q'
            ],[
                'name' => '3-Quarter With White Lowers',
                'abbreviation' => '3W'
            ],[
                'name' => 'Half Battenburg',
                'abbreviation' => 'HB'
            ]
        ]);

        DB::table('end_uses')->insert([
            [
                'name' => 'Police Forces',
                'abbreviation' => 'PF'
            ],[
                'name' => 'Ambulance and Doctors',
                'abbreviation' => 'AM'
            ],[
                'name' => 'Fire and Rescue',
                'abbreviation' => 'FR'
            ],[
                'name' => 'NHS Blood and Translate, Blood Bikes',
                'abbreviation' => 'NB'
            ],[
                'name' => 'Highways England and VOSA',
                'abbreviation' => 'HE'
            ],[
                'name' => 'Rail Response',
                'abbreviation' => 'RR'
            ],[
                'name' => 'Mountain Rescue',
                'abbreviation' => 'MR'
            ],[
                'name' => 'HM Coastguard',
                'abbreviation' => 'HC'
            ],[
                'name' => 'Immigration Enforcement',
                'abbreviation' => 'IE'
            ],[
                'name' => 'Highways England Contractors',
                'abbreviation' => 'HEC'
            ]
        ]);

        DB::table('chevron_variants')->insert([
            [
                'name' => 'Full Chevron',
                'abbreviation' => 'FC'
            ],[
                'name' => 'Half Chevron',
                'abbreviation' => 'HC'
            ],[
                'name' => '3-Quarter Kit Chevron',
                'abbreviation' => '3Q'
            ]
        ]);

        DB::table('escort_variants')->insert([
            [
                'name' => '150mm Chevrons',
                'abbreviation' => '150'
            ],[
                'name' => '250mm Chevrons',
                'abbreviation' => '250'
            ]
        ]);

        DB::table('bonnet_variants')->insert([
            [
                'name' => 'Bonnet Chevrons',
                'abbreviation' => 'BC'
            ],[
                'name' => 'Bonnet Wrap',
                'abbreviation' => 'BW'
            ],[
                'name' => 'Bonnet Flutes',
                'abbreviation' => 'BP'
            ]
        ]);

        DB::table('template_variants')->insert([
            [
                'name' => 'Emergency Service Vehicle 6mm',
                'abbreviation' => 'ESV'
            ],[
                'name' => 'Escort Vehicle 6mm',
                'abbreviation' => 'ES'
            ],[
                'name' => 'Wrap Type 3mm',
                'abbreviation' => 'WT'
            ]
        ]);

        DB::table('grades')->insert([
            [
                'name' => 'Entry Grade',
                'abbreviation' => 'EG'
            ],[
                'name' => 'Intermediate Grade',
                'abbreviation' => 'IG'
            ],[
                'name' => 'Premium Grade',
                'abbreviation' => 'PG'
            ]
        ]);

        DB::table('materials')->insert([
            [
                'name' => '3M Diamond Grade',
                'abbreviation' => '3MD'
            ],[
                'name' => 'Reflexite VC612',
                'abbreviation' => '612'
            ],[
                'name' => 'Oralite 6900',
                'abbreviation' => '6900'
            ]
        ]);

        DB::table('other_properties')->insert([
            [
                'name' => 'Magnetic Backing',
                'abbreviation' => 'MG'
            ],[
                'name' => 'Microskin Backing',
                'abbreviation' => 'MB'
            ],[
                'name' => 'Stone Guard Laminate',
                'abbreviation' => 'SG'
            ],[
                'name' => 'Painted Bumpers',
                'abbreviation' => 'PB'
            ]
        ]);

        

    }
}
