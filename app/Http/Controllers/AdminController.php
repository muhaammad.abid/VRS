<?php

namespace App\Http\Controllers;

use App\Row;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Modal;
use App\OtherData;
use App\RearDoor;
use App\RoofConfiguration;
use App\SideDoorConfiguration;
use App\WheelBase;
use App\Result;
use App\Make;
use App\Year;

use App\BattenbergVariant;
use App\BonnetVariant;
use App\ChevronVariant;
use App\EndUse;
use App\EscortVariant;
use App\Grade;
use App\Materials;
use App\OtherProperty;
use App\TemplateVariant;

class AdminController extends Controller
{
    public function dashboard()
    {
        $rows = Row::orderBy('id', 'desc')->get();
        return view('admin.dashboard',compact('rows'));
    }

    public function results()
    {
        $results = Result::orderBy('id', 'desc')->get();
    	return view('admin.results',compact('results'));
    }

    public function delete_all()
    {
        Result::truncate();
        
        return redirect()->back()->with('success', 'All Result Deleted!');   
    }

    public function single_delete($id)
    {
        Result::where('id', $id)->delete();
        
        return redirect()->back()->with('success', 'Successfully Deleted!');   
    }

    public function changepassword()
    {
        return view('auth.changepassword');
    }

    public function savepassword(Request $request)
    {
        $user = User::findorfail(Auth::user()->id);
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->back()->with('success', 'You password has been changed successfully')->withInput();
    }

    public function model($id)
    {

        $model = Modal::where('make_idFk', $id)->get()->toArray();
//        dd($model);
        return $model;
    }

    public function year($id)
    {
        $year = Year::where('model_idFk', $id)->get()->toArray();
        return $year;
    }

    public function all($id)
    {
        $data = [];
        $data['rear_door'] = RearDoor::where('year_idFk', $id)->get()->toArray();
        $data['wheel_base'] = WheelBase::where('year_idFk', $id)->get()->toArray();
        $data['roof_config'] = RoofConfiguration::where('year_idFk', $id)->get()->toArray();
        $data['side_door_config'] = SideDoorConfiguration::where('year_idFk', $id)->get()->toArray();
        $data['other_data'] = OtherData::where('year_idFk', $id)->get()->toArray();

        return $data;
    }

    public function returnID($id)
    {
    
        return $id;
    }

    public function edit_page($id)
    {
        $result     = Result::findorfail($id);
        $makes      = Make::all();
        $models      = Modal::where('make_idFk', $result->make)->get();
        $years       = Year::where('model_idFk', $result->model)->get();

        if(isset($result->wheel_base)){
            $wheel_base = WheelBase::where('year_idFk', $result->year)->get();
        }
        if(isset($result->roof_config)){
            $roof_config = RoofConfiguration::where('year_idFk', $result->year)->get();
        }
        if(isset($result->side_door_config)){
            $side_door_config = SideDoorConfiguration::where('year_idFk', $result->year)->get();
        }
        if(isset($result->rear_door)){
            $rear_door = RearDoor::where('year_idFk', $result->year)->get();
        }

        $battens    = BattenbergVariant::all();
        $bonnets    = BonnetVariant::all();
        $chevrons   = ChevronVariant::all();
        $enduses    = EndUse::all();
        $escorts    = EscortVariant::all();
        $grades     = Grade::all();
        $materials  = Materials::all();
        $others     = OtherProperty::all();
        $templates  = TemplateVariant::all();

        return view('admin.edit_result', compact('result', 'makes', 'models', 'years', 'wheel_base', 'roof_config', 'side_door_config', 'rear_door', 'battens', 'bonnets','chevrons', 'enduses', 'escorts', 'grades', 'materials', 'others', 'templates'));
    }

    public function exportResult()
    {
        $rows =  Result::get();

        $data = [];

        if(count($rows) > 0){

            foreach ($rows as $key => $value) {

                $data[$key]['ID'] = '#ticket_'.$value->id;
                $data[$key]['Name'] = $value->name;
                $data[$key]['Shortname'] = $value->shortname;
                $data[$key]['Description'] = $value->description;

            }

        }else{
            return back()->with('error', 'Record Not Found!');
        }

        $name = date('Y-m-d H:i').' Results';

        \Excel::create($name, function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download('csv');

    }

}
