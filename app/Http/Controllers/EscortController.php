<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EscortVariant;

class EscortController extends Controller
{
    public function escortPage()
    {
    	$enduse = EscortVariant::all();

    	return view('admin.setting.escort.list', compact('enduse'));
    }

    public function escortAdd()
    {

    	return view('admin.setting.escort.add');
    }

    public function escortSubmit(Request $request)
    {
    	$enduse = new EscortVariant();

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/escort/list')->with('success', 'Added Successfully!');
    }

    public function escortEdit($id)
    {
    	$enduse = EscortVariant::findOrFail($id);

    	return view('admin.setting.escort.edit', compact('enduse'));
    }

    public function escortDelete($id)
    {
    	$enduse = EscortVariant::findOrFail($id);
    	$enduse->delete();
    	return redirect('admin/product/escort/list')->with('success', 'Deleted Successfully!');
    }

    public function escortUpdate(Request $request)
    {

    	$enduse = EscortVariant::findOrFail($request->id);

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/escort/list')->with('success', 'Updated Successfully!');
    }
}
