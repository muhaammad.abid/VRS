<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grade;

class GradeController extends Controller
{
    public function gradePage()
    {
    	$enduse = Grade::all();

    	return view('admin.setting.grade.list', compact('enduse'));
    }

    public function gradeAdd()
    {

    	return view('admin.setting.grade.add');
    }

    public function gradeSubmit(Request $request)
    {
    	$enduse = new Grade();

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/grade/list')->with('success', 'Added Successfully!');
    }

    public function gradeEdit($id)
    {
    	$enduse = Grade::findOrFail($id);

    	return view('admin.setting.grade.edit', compact('enduse'));
    }

    public function gradeDelete($id)
    {
    	$enduse = Grade::findOrFail($id);
    	$enduse->delete();
    	return redirect('admin/product/grade/list')->with('success', 'Deleted Successfully!');
    }

    public function gradeUpdate(Request $request)
    {

    	$enduse = Grade::findOrFail($request->id);

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/grade/list')->with('success', 'Updated Successfully!');
    }
}
