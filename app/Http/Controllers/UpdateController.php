<?php

namespace App\Http\Controllers;

use App\Make;
use App\Modal;
use App\OtherData;
use App\RearDoor;
use App\RoofConfiguration;
use App\SideDoorConfiguration;
use App\WheelBase;
use App\Year;
use App\Result;
use App\Http\Controllers\IndexController;
use Illuminate\Http\Request;


class UpdateController extends Controller
{
    public function updateResult(Request $request)
    {
    	$data = [];
        $new = Result::findOrFail($request->result_id);


        $data['brand']  = $request->brand;
        $data['make']   = Make::where('id', $request->make)->first();
        $data['model']  = Modal::where('id', $request->model)->first();
        $data['year']   = Year::where('id', $request->year)->first();

        $new->brand  = $request->brand;
        $new->make   = $request->make;
        $new->model  = $request->model;
        $new->year   = $request->year;

        if($request->wheel_base){
            $data['wheel_base'] = WheelBase::where('id', $request->wheel_base)->first();
            $new->wheel_base   = $request->wheel_base;    
        }
        if ($request->roof_config) {
            $data['roof_config'] = RoofConfiguration::where('id', $request->roof_config)->first();
            $new->roof_config   = $request->roof_config;
        }
        if ($request->side_door_config) {
            $data['side_door_config'] = SideDoorConfiguration::where('id', $request->side_door_config)->first();
            $new->side_door_config   = $request->side_door_config;
        }
        if ($request->rear_door) {
            $data['rear_door'] = RearDoor::where('id', $request->rear_door)->first();
            $new->rear_door   = $request->rear_door;
        }
        if ($request->parking == 'on') {
            
            $data['parking'][0] = 'Parking Sensors';
            $data['parking'][1] = 'PS';
            $new->parking   = 'on';
            
        }else{
            $new->parking = 'off';
        }
        
        if ($request->magnetic == 'on') {
            $data['magnetic'][0] = 'Magnetic Backing';
            $data['magnetic'][1] = 'MG';
            $new->magnetic   = 'on';
        }else{
        	$new->magnetic   = 'off';
        }
        if ($request->microskin == 'on') {
            $data['microskin'][0] = 'Microskin Backing';
            $data['microskin'][1] = 'MS';
            $new->microskin   = 'on';
        }else{
        	$new->microskin   = 'off';
        }
        if ($request->stone_guard == 'on') {
            $data['stone_guard'][0] = 'Stone Guard Laminate';
            $data['stone_guard'][1] = 'SG';
            $new->stone_guard   = 'on';
        }else{
        	$new->stone_guard   = 'off';
        }
        if ($request->painted == 'on') {
            $data['painted'][0] = 'Painted Bumpers';
            $data['painted'][1] = 'PB';
            $new->painted   = 'on';
        }else{
        	$new->painted   = 'off';
        }
        if ($request->variant) {
            $variant = explode('(', $request->variant);
            $data['variant'] = $variant;
            $new->variant   = $request->variant;
        }else{

        }
            
        if ($request->material) {
            $material = explode('(', $request->material);
            $data['material'] = $material;
            $new->material   = $request->material;
        }
        if ($request->grade) {
            $grade = explode('(', $request->grade);
            $data['grade'] = $grade;
            $new->grade   = $request->grade;
        }
        if ($request->end_user) {
            $end_user = explode('(', $request->end_user);
            $data['end_user'] = $end_user;
            $new->end_use   = $request->end_user;
        }
        


        $result = IndexController::generator($data);

        $new->name          = $result['name'];
        $new->shortname     = $result['shortname'];
        $new->description   = $result['description'];

        $new->save();

        return view('admin.generate', compact('new', 'result'));
    }
}
