<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BattenbergVariant;

class SettingController extends Controller
{
    public function settingPage()
    {
    	$battens = BattenbergVariant::all();

    	return view('admin.setting.index', compact('battens'));
    }

    public function battenAdd()
    {

    	return view('admin.setting.batten.add');
    }

    public function battenSubmit(Request $request)
    {

    	$batten = new BattenbergVariant();

    	$batten->name = $request->name;
    	$batten->abbreviation = $request->abbreviation;

    	$batten->save();

    	return redirect('admin/product/settings')->with('success', 'Added Successfully!');
    }

    public function battenEdit($id)
    {
    	$batten = BattenbergVariant::findOrFail($id);

    	return view('admin.setting.batten.edit', compact('batten'));
    }

    public function battenDelete($id)
    {
    	$batten = BattenbergVariant::findOrFail($id);
    	$batten->delete();
    	return redirect('admin/product/settings')->with('success', 'Deleted Successfully!');
    }

    public function battenUpdate(Request $request)
    {

    	$batten = BattenbergVariant::findOrFail($request->id);

    	$batten->name = $request->name;
    	$batten->abbreviation = $request->abbreviation;

    	$batten->save();

    	return redirect('admin/product/settings')->with('success', 'Updated Successfully!');
    }
}
