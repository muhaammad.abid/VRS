<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChevronVariant;

class ChevronController extends Controller
{
    public function chevronPage()
    {
    	$enduse = ChevronVariant::all();

    	return view('admin.setting.chevron.list', compact('enduse'));
    }

    public function chevronAdd()
    {

    	return view('admin.setting.chevron.add');
    }

    public function chevronSubmit(Request $request)
    {
    	$enduse = new ChevronVariant();

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/chevron/list')->with('success', 'Added Successfully!');
    }

    public function chevronEdit($id)
    {
    	$enduse = ChevronVariant::findOrFail($id);

    	return view('admin.setting.chevron.edit', compact('enduse'));
    }

    public function chevronDelete($id)
    {
    	$enduse = ChevronVariant::findOrFail($id);
    	$enduse->delete();
    	return redirect('admin/product/chevron/list')->with('success', 'Deleted Successfully!');
    }

    public function chevronUpdate(Request $request)
    {

    	$enduse = ChevronVariant::findOrFail($request->id);

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/chevron/list')->with('success', 'Updated Successfully!');
    }
}
