<?php

namespace App\Http\Controllers;

use App\Make;
use App\Modal;
use App\OtherData;
use App\RearDoor;
use App\RoofConfiguration;
use App\SideDoorConfiguration;
use App\WheelBase;
use App\Year;
use App\Result;
use Illuminate\Http\Request;

use App\BattenbergVariant;
use App\BonnetVariant;
use App\ChevronVariant;
use App\EndUse;
use App\EscortVariant;
use App\Grade;
use App\Materials;
use App\OtherProperty;
use App\TemplateVariant;

class IndexController extends Controller
{
    public function check(Request $request)
    {
        $makes = Make::all();
        if(count($makes) == 0){
            return back()->with('error', 'Data Not Found!');
        }
        
        $battens    = BattenbergVariant::all();
        $bonnets    = BonnetVariant::all();
        $chevrons   = ChevronVariant::all();
        $enduses    = EndUse::all();
        $escorts    = EscortVariant::all();
        $grades     = Grade::all();
        $materials  = Materials::all();
        $others     = OtherProperty::all();
        $templates  = TemplateVariant::all();


        return view('front.form',compact('request','makes', 'battens', 'bonnets','chevrons', 'enduses', 'escorts', 'grades', 'materials', 'others', 'templates'));
    }

    public function get_models($id)
    {
        $model = Modal::where('make_idFk', $id)->get()->toArray();
        return $model;
    }

    public function get_years($id)
    {
        $year = Year::where('model_idFk', $id)->get()->toArray();
        return $year;
    }

    public function all($id)
    {
        $data = [];
        $data['rear_door'] = RearDoor::where('year_idFk', $id)->get()->toArray();
        $data['wheel_base'] = WheelBase::where('year_idFk', $id)->get()->toArray();
        $data['roof_config'] = RoofConfiguration::where('year_idFk', $id)->get()->toArray();
        $data['side_door_config'] = SideDoorConfiguration::where('year_idFk', $id)->get()->toArray();
        $data['other_data'] = OtherData::where('year_idFk', $id)->get()->toArray();

        return $data;
    }

    public function form_data(Request $request)
    {
        $data = [];
        $new = new Result();


        $data['brand']  = $request->brand;
        $data['make']   = Make::where('id', $request->make)->first();
        $data['model']  = Modal::where('id', $request->model)->first();
        $data['year']   = Year::where('id', $request->year)->first();

        $new->brand  = $request->brand;
        $new->make   = $request->make;
        $new->model  = $request->model;
        $new->year   = $request->year;

        if($request->wheel_base){
            $data['wheel_base'] = WheelBase::where('id', $request->wheel_base)->first();
            $new->wheel_base   = $request->wheel_base;    
        }
        if ($request->roof_config) {
            $data['roof_config'] = RoofConfiguration::where('id', $request->roof_config)->first();
            $new->roof_config   = $request->roof_config;
        }
        if ($request->side_door_config) {
            $data['side_door_config'] = SideDoorConfiguration::where('id', $request->side_door_config)->first();
            $new->side_door_config   = $request->side_door_config;
        }
        if ($request->rear_door) {
            $data['rear_door'] = RearDoor::where('id', $request->rear_door)->first();
            $new->rear_door   = $request->rear_door;
        }
        if ($request->parking == 'on') {
            
            $data['parking'][0] = 'Parking Sensors';
            $data['parking'][1] = 'PS';
            $new->parking   = 'on';
            
        }
        
        if ($request->magnetic == 'on') {
            $data['magnetic'][0] = 'Magnetic Backing';
            $data['magnetic'][1] = 'MG';
            $new->magnetic   = 'on';
        }
        if ($request->microskin == 'on') {
            $data['microskin'][0] = 'Microskin Backing';
            $data['microskin'][1] = 'MS';
            $new->microskin   = 'on';
        }
        if ($request->stone_guard == 'on') {
            $data['stone_guard'][0] = 'Stone Guard Laminate';
            $data['stone_guard'][1] = 'SG';
            $new->stone_guard   = 'on';
        }
        if ($request->painted == 'on') {
            $data['painted'][0] = 'Painted Bumpers';
            $data['painted'][1] = 'PB';
            $new->painted   = 'on';
        }
        if ($request->variant) {
            $variant = explode('(', $request->variant);
            $data['variant'] = $variant;
            $new->variant   = $request->variant;
        }
            
        if ($request->material) {
            $material = explode('(', $request->material);
            $data['material'] = $material;
            $new->material   = $request->material;
        }
        if ($request->grade) {
            $grade = explode('(', $request->grade);
            $data['grade'] = $grade;
            $new->grade   = $request->grade;
        }
        if ($request->end_user) {
            $end_user = explode('(', $request->end_user);
            $data['end_user'] = $end_user;
            $new->end_use   = $request->end_user;
        }
        

        $result = self::generator($data);

        $new->name          = $result['name'];
        $new->shortname     = $result['shortname'];
        $new->description   = $result['description'];

        $new->save();


        return view('front.generate', compact('result', 'new'));
    }

    static function generator($data)
    {
        $name = '';
        $shortname = '';
        $description = '';
        
        if($data['brand'] == 1){

            // name for 1...
            if(isset($data['variant'][0])){
                $name .= trim($data['variant'][0]).' ';
            }

            $name .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';
            if(isset($data['wheel_base']) && $data['wheel_base']->name != 'N/A')
                $name .= $data['wheel_base']->name.' ';
            
            if(isset($data['roof_config']) && $data['roof_config']->name != 'N/A')
                $name .= $data['roof_config']->name.' ';   

            if(isset($data['side_door_config']) && $data['side_door_config']->name != 'N/A')
                $name .= $data['side_door_config']->name.' ';
            
            if(isset($data['rear_door']) && $data['rear_door']->name != 'N/A')
                $name .= $data['rear_door']->name.' '; 
            
            
            if(isset($data['parking'][0]))
                $name .= trim($data['parking'][0]).' '; 
             
            if(isset($data['painted']))
                $name .= trim($data['painted'][0]).' ';
            
            if(isset($data['end_user'][0]))
                $name .= trim($data['end_user'][0]).' '; 
            
            if(isset($data['material'][0]))
                $name .= trim($data['material'][0]).' '; 
            
            if(isset($data['magnetic']))
                $name .= trim($data['magnetic'][0]).' ';
            
            if(isset($data['microskin']))
                $name .= trim($data['microskin'][0]).' ';
            
            if(isset($data['stone_guard']))
                $name .= trim($data['stone_guard'][0]).' ';

            if(isset($data['painted']))
                $name .= trim($data['painted'][0]).' ';


            // shrot name for 1...


            if(isset($data['variant'][1])){
                $shortname .= trim($data['variant'][1]).' ';
            }

            $shortname .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            if(isset($data['wheel_base']) && $data['wheel_base']->abbreviation != 'N/A')
                $shortname .= $data['wheel_base']->abbreviation.' ';
            
            if(isset($data['roof_config'])  && $data['roof_config']->abbreviation != 'N/A')
                $shortname .= $data['roof_config']->abbreviation.' ';   
            
            if(isset($data['side_door_config'])  && $data['side_door_config']->abbreviation != 'N/A')
                $shortname .= $data['side_door_config']->abbreviation.' ';

            if(isset($data['rear_door'])  && $data['rear_door']->abbreviation != 'N/A')
                $shortname .= $data['rear_door']->abbreviation.' '; 
            
            
            if(isset($data['parking'][1]))
                $shortname .= trim($data['parking'][1]).' '; 
             
            if(isset($data['painted']))
                $shortname .= trim($data['painted'][1]).' ';
            
            if(isset($data['end_user'][1]))
                $shortname .= trim($data['end_user'][1]).' '; 
            
            if(isset($data['material'][1]))
                $shortname .= trim($data['material'][1]).' '; 
            
            if(isset($data['magnetic']))
                $shortname .= trim($data['magnetic'][1]).' ';
            
            if(isset($data['microskin']))
                $shortname .= trim($data['microskin'][1]).' ';
            
            if(isset($data['stone_guard']))
                $shortname .= trim($data['stone_guard'][1]).' ';

            if(isset($data['painted']))
                $shortname .= trim($data['painted'][1]).' ';

            // description for 1...

            if(isset($data['variant'][0])){
                $description .= trim($data['variant'][0]).' ';
            }

            if(isset($data['end_user'][0]))
                $description .= trim($data['end_user'][0]).'. '; 

            $description .= 'Materials to be used ';

            if(isset($data['material'][0]))
                $description .= trim($data['material'][0]).'. '; 

            $description .= 'The kit is designed to fit ';

            $description .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            if(isset($data['wheel_base']) && $data['wheel_base']->name != 'N/A')
                $description .= $data['wheel_base']->name.' ';

            if(isset($data['side_door_config']) && $data['side_door_config']->name != 'N/A')
                $description .= $data['side_door_config']->name.' ';

            if(isset($data['roof_config']) && $data['roof_config']->name != 'N/A')
                $description .= $data['roof_config']->name.' ';   

            
            if(isset($data['rear_door']) && $data['rear_door']->name != 'N/A')
                $description .= $data['rear_door']->name.' ';

            if(isset($data['painted']))
                $description .= trim($data['painted'][0]).' ';

            if(isset($data['parking'][0]))
                $description .= trim($data['parking'][0]).' ';

            if(isset($data['magnetic']) )
                $description .= trim($data['magnetic'][0]).' ';
            
            if(isset($data['microskin']))
                $description .= trim($data['microskin'][0]).' ';
            
            if(isset($data['stone_guard']))
                $description .= trim($data['stone_guard'][0]).' '; 

            if(isset($data['painted']))
                $description .= trim($data['painted'][0]).' ';
              
        
            
        }

        
        if($data['brand'] == 2){

            // name for 1...

            $name .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            if(isset($data['roof_config']) && $data['roof_config']->name != 'N/A')
                $name .= $data['roof_config']->name.' ';

            if(isset($data['rear_door']) && $data['rear_door']->name != 'N/A')
                $name .= $data['rear_door']->name.' '; 

            if(isset($data['variant'][0])){
                $name .= trim($data['variant'][0]).' ';
            }

            if(isset($data['grade'][0])) 
                $name .= trim($data['grade'][0]).' ';
        


            // shrot name for 1...


            $shortname .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            if(isset($data['roof_config']) && $data['roof_config']->abbreviation != 'N/A')
                $shortname .= $data['roof_config']->abbreviation.' ';

            if(isset($data['rear_door']) && $data['rear_door']->abbreviation != 'N/A')
                $shortname .= $data['rear_door']->abbreviation.' ';

            if(isset($data['variant'][1])){
                $shortname .= trim($data['variant'][1]).' ';
            }

            if(isset($data['grade'][1])) 
                $shortname .= trim($data['grade'][1]).' '; 

            // description for 1...

            if(isset($data['variant'][0]) ){
                $description .= trim($data['variant'][0]).' ';
            }

            $description .= 'chapter 8 kit designed to fit ';

            $description .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            if(isset($data['roof_config']) && $data['roof_config']->name != 'N/A')
                $description .= $data['roof_config']->name.' ';

            if(isset($data['rear_door']) && $data['rear_door']->name != 'N/A')
                $description .= $data['rear_door']->name.', '; 

            if(isset($data['grade'][0])) 
                $description .= trim($data['grade'][0]).'. '; 
              
            $description .= 'This kit provide coverage ';

            if(isset($data['variant'][0]) ){
                if(trim($data['variant'][0]) == 'Full Chevron'){
                    $description .= 'including ';
                }
                if(trim($data['variant'][0]) == '3-Quarter Kit Chevron'){
                    $description .= 'excluding ';
                }
                if(trim($data['variant'][0]) == 'Half Chevron'){
                    $description .= 'up to ';
                }
            }

            $description .= 'the rear windows panels.';
            
        }

        if($data['brand'] == 3){

            // name for 1...

            
            $name .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            if(isset($data['wheel_base']) && $data['wheel_base']->name != 'N/A')
                $name .= $data['wheel_base']->name.' ';


            if(isset($data['roof_config']) && $data['roof_config']->name != 'N/A')
                $name .= $data['roof_config']->name.' '; 
            
            if(isset($data['side_door_config']) && $data['side_door_config']->name != 'N/A')
                $name .= $data['side_door_config']->name.' '; 

            if(isset($data['rear_door']) && $data['rear_door']->name != 'N/A')
                $name .= $data['rear_door']->name.' '; 

            if(isset($data['magnetic']))
                $name .= trim($data['magnetic'][0]).' ';
            
            if(isset($data['microskin']))
                $name .= trim($data['microskin'][0]).' ';
            
            if(isset($data['stone_guard']))
                $name .= trim($data['stone_guard'][0]).' ';

            if(isset($data['painted']))
                $name .= trim($data['painted'][0]).' ';

            $name .= 'Escort Vehicle ';

            if(isset($data['material'][0])){
                $name .= trim($data['material'][0]).' ';
            }
            
            if(isset($data['variant'][0])){
                $name .= trim($data['variant'][0]).' ';
            }


            // short name for 1...
            
            $shortname .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            $shortname .= 'Escort Vehicle ';

            if(isset($data['magnetic']))
                $shortname .= trim($data['magnetic'][1]).' ';
            
            if(isset($data['microskin']))
                $shortname .= trim($data['microskin'][1]).' ';
            
            if(isset($data['stone_guard']))
                $shortname .= trim($data['stone_guard'][1]).' ';

            if(isset($data['painted']))
                $shortname .= trim($data['painted'][1]).' ';

            if(isset($data['variant'][1])){
                $shortname .= trim($data['variant'][1]).' ';
            }

            // if(isset($data['wheel_base']) && $data['wheel_base']->abbreviation != 'N/A')
            //     $shortname .= $data['wheel_base']->abbreviation.' ';

            // if(isset($data['roof_config']) && $data['roof_config']->abbreviation != 'N/A')
            //     $shortname .= $data['roof_config']->abbreviation.' '; 
            
            // if(isset($data['side_door_config']) && $data['side_door_config']->abbreviation != 'N/A')
            //     $shortname .= $data['side_door_config']->abbreviation.' '; 

            // if(isset($data['rear_door']) && $data['rear_door']->abbreviation != 'N/A')
            //     $shortname .= $data['rear_door']->abbreviation.' '; 


            // if(isset($data['material'][1])){
            //     $shortname .= $data['material'][1].' ';
            // }

            // description for 1...

            $description .= 'Escort Vehicle kit designed to fit ';

            $description .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            if(isset($data['wheel_base']) && $data['wheel_base']->name != 'N/A')
                $description .= $data['wheel_base']->name.' ';

            if(isset($data['roof_config']) && $data['roof_config']->name != 'N/A')
                $description .= $data['roof_config']->name.' '; 
            
            if(isset($data['side_door_config']) && $data['side_door_config']->name != 'N/A')
                $description .= $data['side_door_config']->name.' '; 

            if(isset($data['rear_door']) && $data['rear_door']->name != 'N/A')
                $description .= $data['rear_door']->name.' '; 

            if(isset($data['magnetic']))
                $description .= trim($data['magnetic'][0]).' ';
            
            if(isset($data['microskin']))
                $description .= trim($data['microskin'][0]).' ';
            
            if(isset($data['stone_guard']))
                $description .= trim($data['stone_guard'][0]).' ';

            if(isset($data['painted']))
                $description .= trim($data['painted'][0]).' ';

            $description .= '. ';

            if(isset($data['material'][0])){
                $description .= trim($data['material'][0]).', ';
            }

            if(isset($data['variant'][0])){
                $description .= trim($data['variant'][0]).'. ';
            }

             
              
        
            
        }

        if($data['brand'] == 4){

            // name for 1...
            $name .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            if(isset($data['wheel_base']) &&  $data['wheel_base']->name != 'N/A')
                $name .= $data['wheel_base']->name.' ';

            $name .= 'Side Chevrons ';

            if(isset($data['grade'][0])) 
                $name .= trim($data['grade'][0]).' ';


            // shrot name for 1...
            $shortname .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';
            if(isset($data['wheel_base']) && $data['wheel_base']->abbreviation != 'N/A')
                $shortname .= $data['wheel_base']->abbreviation.' ';

            if(isset($data['grade'][1])) 
                $shortname .= trim($data['grade'][1]).' ';



            // description for 1...
            $description .= 'Side Chevrons designed to fit ';

            $description .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            if(isset($data['wheel_base'])  &&  $data['wheel_base']->name != 'N/A')
                $description .= $data['wheel_base']->name.', ';

            if(isset($data['grade'][0])) 
                $description .= trim($data['grade'][0]).' ';
        }



        if($data['brand'] == 5){

            // name for 1...
            $name .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';
            if(isset($data['variant'][0])){
                $name .= trim($data['variant'][0]).' ';
            }

            if(isset($data['grade'][0])) 
                $name .= trim($data['grade'][0]).' ';


            // shrot name for 1...


            $shortname .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';
            if(isset($data['variant'][1])){
                $shortname .= trim($data['variant'][1]).' ';
            }

            if(isset($data['grade'][1])) 
                $shortname .= trim($data['grade'][1]).' ';

            // description for 1...
            if(isset($data['variant'][0])){
                $description .= trim($data['variant'][0]).' ';
            }

            $description .= 'designed to fit ';

            $description .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.', ';

            if(isset($data['grade'][0])) 
                $description .= trim($data['grade'][0]).' ';
        }


        if($data['brand'] == 6){
           
            // name for 1...

            $name .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';


            if(isset($data['wheel_base']) && $data['wheel_base']->name != 'N/A')
                $name .= $data['wheel_base']->name.' ';

            if(isset($data['roof_config']) && $data['roof_config']->name != 'N/A')
                $name .= $data['roof_config']->name.' ';

            if(isset($data['side_door_config']) && $data['side_door_config']->name != 'N/A')
                $name .= $data['side_door_config']->name.' ';

            if(isset($data['rear_door']) && $data['rear_door']->name != 'N/A')
                $name .= $data['rear_door']->name.' ';

            
            $name .= 'Template ';

            if(isset($data['variant'][0])){
                $name .= trim($data['variant'][0]).' ';
            }


            // shrot name for 1...


            $shortname .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';

            $shortname .= 'Template ';

            if(isset($data['variant'][1])){
                $shortname .= trim($data['variant'][1]).' ';
            }

            // if(isset($data['wheel_base']) && $data['wheel_base']->abbreviation != 'N/A')
            //     $shortname .= $data['wheel_base']->abbreviation.' ';

            // if(isset($data['roof_config']) && $data['roof_config']->abbreviation != 'N/A')
            //     $shortname .= $data['roof_config']->abbreviation.' ';

            // if(isset($data['side_door_config']) && $data['side_door_config']->abbreviation != 'N/A')
            //     $shortname .= $data['side_door_config']->abbreviation.' ';

            // if(isset($data['rear_door']) && $data['rear_door']->abbreviation != 'N/A')
            //     $shortname .= $data['rear_door']->abbreviation.' ';



            // description name for 1...

            $description .= 'Template designed for ';


            $description .= $data['make']->name .' '. $data['model']->name .' '. $data['year']->name.' ';


            if(isset($data['wheel_base']) && $data['wheel_base']->name != 'N/A')
                $description .= $data['wheel_base']->name.' ';

            if(isset($data['roof_config']) && $data['roof_config']->name != 'N/A')
                $description .= $data['roof_config']->name.' ';

            if(isset($data['side_door_config']) && $data['side_door_config']->name != 'N/A')
                $description .= $data['side_door_config']->name.' ';

            if(isset($data['rear_door']) && $data['rear_door']->name != 'N/A')
                $description .= $data['rear_door']->name.' ';


            if(isset($data['variant'][0])){
                $description .= trim($data['variant'][0]).' ';
            }

        }


        $result['name'] = $name;
        $result['shortname'] = $shortname;
        $result['description'] = $description;

        return $result;

    }

    public function form()
    {
        return view('front.form');
    }
}
