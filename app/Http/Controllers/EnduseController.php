<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EndUse;

class EnduseController extends Controller
{
    public function endusePage()
    {
    	$enduse = EndUse::all();

    	return view('admin.setting.enduse.list', compact('enduse'));
    }

    public function enduseAdd()
    {

    	return view('admin.setting.enduse.add');
    }

    public function enduseSubmit(Request $request)
    {
    	$enduse = new EndUse();

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/enduse/list')->with('success', 'Added Successfully!');
    }

    public function enduseEdit($id)
    {
    	$enduse = EndUse::findOrFail($id);

    	return view('admin.setting.enduse.edit', compact('enduse'));
    }

    public function enduseDelete($id)
    {
    	$enduse = EndUse::findOrFail($id);
    	$enduse->delete();
    	return redirect('admin/product/enduse/list')->with('success', 'Deleted Successfully!');
    }

    public function enduseUpdate(Request $request)
    {

    	$enduse = EndUse::findOrFail($request->id);

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/enduse/list')->with('success', 'Updated Successfully!');
    }
}
