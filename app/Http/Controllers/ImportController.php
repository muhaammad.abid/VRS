<?php

namespace App\Http\Controllers;

use App\SideDoorConfiguration;
use Illuminate\Http\Request;
use App\Make;
use App\Modal;
use App\Year;
use App\WheelBase;
use App\RoofConfiguration;
use App\RearDoor;
use App\OtherData;
use App\Row;

class ImportController extends Controller
{
    public function importExcel(Request $request)
    {
        //if user clear older data
        if($request->clear == 'clear')
        {
            Row::truncate();

            $years = Year::all();
            if(count($years) > 0){
                foreach($years as $year){
                    OtherData::where('year_idFk', $year->id)->delete();
                    RearDoor::where('year_idFk', $year->id)->delete();
                    RoofConfiguration::where('year_idFk', $year->id)->delete();
                    WheelBase::where('year_idFk', $year->id)->delete();
                }
            }

            $models = Modal::all();
            if(count($models) > 0){
                foreach($models as $model){
                    Year::where('model_idFk', $model->id)->delete();
                }
            }

            $makes = Make::all();
            if(count($makes) > 0){
                foreach($makes as $make){
                    Modal::where('make_idFk', $make->id)->delete();
                    Make::where('id', $make->id)->delete();
                }
            }
        }  

        
        if($request->hasFile('import_file'))
        {
            $path = $request->file('import_file')->getRealPath();
            $data = \Excel::load($path, function($reader) {
            })->get();

       
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $val) {

                        if(!Row::where('make', $val->make)->where('model', $val->model)->where('year', $val->generation_year)
                                ->where('wheel_base', $val->wheel_base)->where('wb_abbreviation', $val->wb_abbreviation)
                                ->where('roof_config', $val->roof_configuration)->where('rc_abbreviation', $val->rc_abbreviation)
                                ->where('side_door_config', $val->side_door_config)->where('sdc_abbreviation', $val->sdc_abbreviation)
                                ->where('rear_door_config', $val->rear_doors_config)->where('rdc_abbreviation', $val->rdc_abbreviation)
                                ->where('parking', $val->parking_sensors_present)->where('painted', $val->painted_bumpers)
                                ->where('trim', $val->variant_trim)->where('type', $val->vehice_type)
                                ->where('other', $val->other_info)->exists()){

                            $row = new Row();
                            $row->make      = $val->make;
                            $row->model      = $val->model;
                            $row->year      = $val->generation_year;
                            $row->wheel_base      = $val->wheel_base;
                            $row->wb_abbreviation      = $val->wb_abbreviation;
                            $row->roof_config      = $val->roof_configuration;
                            $row->rc_abbreviation      = $val->rc_abbreviation;
                            $row->side_door_config      = $val->side_door_config;
                            $row->sdc_abbreviation      = $val->sdc_abbreviation;
                            $row->rear_door_config      = $val->rear_doors_config;
                            $row->rdc_abbreviation      = $val->rdc_abbreviation;
                            $row->parking      = $val->parking_sensors_present;
                            $row->painted      = $val->painted_bumpers;
                            $row->trim      = $val->variant_trim;
                            $row->type     = $val->vehice_type;
                            $row->other      = $val->other_info;
                            $row->save();
                            
                        }
                        

                        if(Make::where('name', $val->make)->exists()){
                            $make = Make::where('name', $val->make)->first();
                        }else{
                            $make = new Make();
                            $make->name = $val->make;
                            $make->save();
                        }

                        if(Modal::where('name', $val->model)->where('make_idFk', $make->id)->exists()){
                            $model = Modal::where('name', $val->model)->where('make_idFk', $make->id)->first();
                        }else{
                            $model = new Modal();
                            $model->name = $val->model;
                            $model->make_idFk = $make->id;
                            $model->save();
                        }

                        if(Year::where('name', $val->generation_year)->where('model_idFk', $model->id)->exists()){
                            $year = Year::where('name', $val->generation_year)->where('model_idFk', $model->id)->first();
                        }else{

                            $year = new Year();
                            $year->name = $val->generation_year;
                            $year->model_idFk = $model->id;
                            $year->save();
                        }

                        if(WheelBase::where('name', $val->wheel_base)->where('year_idFk', $year->id)->exists()){
                            $wheelbase = WheelBase::where('name', $val->wheel_base)->where('year_idFk', $year->id)->first();
                        }else{
                            $wheelbase = new WheelBase();
                            $wheelbase->name = $val->wheel_base;
                            $wheelbase->year_idFk = $year->id;
                            $wheelbase->abbreviation = $val->wb_abbreviation;
                            $wheelbase->save();
                        }

                        if(RoofConfiguration::where('name', $val->roof_configuration)->where('year_idFk', $year->id)->exists()){
                            $roof_configuration = RoofConfiguration::where('name', $val->roof_configuration)->where('year_idFk', $year->id)->first();
                        }else{

                            $roof_configuration = new RoofConfiguration();
                            $roof_configuration->name = $val->roof_configuration;
                            $roof_configuration->year_idFk =  $year->id;
                            $roof_configuration->abbreviation =  $val->rc_abbreviation;
                            $roof_configuration->save();

                        }

                        if(SideDoorConfiguration::where('name', $val->side_door_config)->where('year_idFk', $year->id)->exists()){
                            $side_door = SideDoorConfiguration::where('name', $val->side_door_config)->where('year_idFk', $year->id)->first();
                        }else{
                            $side_door = new SideDoorConfiguration();
                            $side_door->name = $val->side_door_config;
                            $side_door->year_idFk =  $year->id;
                            $side_door->abbreviation =  $val->sdc_abbreviation;
                            $side_door->save();

                        }

                        if(RearDoor::where('name', $val->rear_doors_config)->where('year_idFk', $year->id)->exists()){
                            $rear_doors = RearDoor::where('name', $val->rear_doors_config)->where('year_idFk', $year->id)->first();
                        }else{
                            $rear_doors = new RearDoor();
                            $rear_doors->name = $val->rear_doors_config;
                            $rear_doors->abbreviation =  $val->rdc_abbreviation;
                            $rear_doors->year_idFk =  $year->id;
                            $rear_doors->save();

                        }

                        if(OtherData::where('parking', $val->parking_sensors_present)->where('painted', $val->painted_bumpers)->where('trim', $val->variant_trim)->where('type', $val->vehice_type)->where('year_idFk', $year->id)->exists()){
                            $other = OtherData::where('parking', $val->parking_sensors_present)->where('painted', $val->painted_bumpers)->where('trim', $val->variant_trim)->where('type', $val->vehice_type)->where('year_idFk', $year->id)->first();
                        }else{

                            $other = new OtherData();
                            $other->year_idFk =  $year->id;
                            $other->parking =  $val->parking_sensors_present;
                            $other->painted =  $val->painted_bumpers;
                            $other->trim =  $val->variant_trim;
                            $other->type =  $val->vehice_type;
                            if($val->other_info){
                                $other->other =  $val->other_info;
                            }else{
                                $other->other =  'N/A';
                            }
                            $other->save();



                        }
                }
//                if(!empty($insert)){
//                    DB::table('items')->insert($insert);
//                    dd('Insert Record successfully.');
//                }
            }
        }
        return back();
    }
}
