<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;

use App\Modal;
use Maatwebsite\Excel\Excel;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest')->except('logout');
    }

    public function forget_page()
    {

        return view('auth.forget');
    }

    public function forget_submit(Request $request)
    {
        if(!User::where('email', $request->email)->exists()){
            return back()->with('error', 'Email address not exist!');
        }

        $user = User::where('email', $request->email)->first();

        $password = str_random(8);

        $user->password = Hash::make($password);
        $user->save();

        $view               = 'mail.forget';
        $data['user']       = User::findOrFail($user->id);
        $data['password']   = $password;
        $subject            = 'New Password';
        
        \Mail::send($view, $data, function ($message) use($user, $subject){
            $message->to($user->email)->subject($subject);
        });

        return redirect('login')->with('success', 'Please check your email for new password.');
    }

    public function login_page()
    {

        return view('auth.login');
    }

    public function authenticateUser(Request $request)
    {
        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ])){

            if(Auth::User()->user_role_idFk == 1){
                return redirect('admin/dashboard');
            }else{
                Auth::logout();
                 return redirect()->back()->with('error', 'Invalid credentials please verify before login')->withInput();

            }
        }else{

            return redirect()->back()->with('error', 'Invalid credentials please verify before login')->withInput();
            
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect('login');
    }



    public function importExport()
    {
        return view('importExport');
    }




}
