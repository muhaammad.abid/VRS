<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OtherProperty;

class OtherController extends Controller
{
    public function otherPage()
    {
    	$enduse = OtherProperty::all();

    	return view('admin.setting.other.list', compact('enduse'));
    }

    public function otherAdd()
    {

    	return view('admin.setting.other.add');
    }

    public function otherSubmit(Request $request)
    {
    	$enduse = new OtherProperty();

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/other/list')->with('success', 'Added Successfully!');
    }

    public function otherEdit($id)
    {
    	$enduse = OtherProperty::findOrFail($id);

    	return view('admin.setting.other.edit', compact('enduse'));
    }

    public function otherDelete($id)
    {
    	$enduse = OtherProperty::findOrFail($id);
    	$enduse->delete();
    	return redirect('admin/product/other/list')->with('success', 'Deleted Successfully!');
    }

    public function otherUpdate(Request $request)
    {

    	$enduse = OtherProperty::findOrFail($request->id);

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/other/list')->with('success', 'Updated Successfully!');
    }
}
