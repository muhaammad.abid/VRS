<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materials;

class MaterialController extends Controller
{
    public function materialPage()
    {
    	$enduse = Materials::all();

    	return view('admin.setting.material.list', compact('enduse'));
    }

    public function materialAdd()
    {

    	return view('admin.setting.material.add');
    }

    public function materialSubmit(Request $request)
    {
    	$enduse = new Materials();

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/material/list')->with('success', 'Added Successfully!');
    }

    public function materialEdit($id)
    {
    	$enduse = Materials::findOrFail($id);

    	return view('admin.setting.material.edit', compact('enduse'));
    }

    public function materialDelete($id)
    {
    	$enduse = Materials::findOrFail($id);
    	$enduse->delete();
    	return redirect('admin/product/material/list')->with('success', 'Deleted Successfully!');
    }

    public function materialUpdate(Request $request)
    {

    	$enduse = Materials::findOrFail($request->id);

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/material/list')->with('success', 'Updated Successfully!');
    }
}
