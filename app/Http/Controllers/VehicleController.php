<?php

namespace App\Http\Controllers;

use App\Make;
use Illuminate\Http\Request;
use App\Modal;
use App\OtherData;
use App\RearDoor;
use App\RoofConfiguration;
use App\SideDoorConfiguration;
use App\WheelBase;
use App\Year;
use App\Row;

class VehicleController extends Controller
{
    public function vehicle_add()
    {
        $makes = Make::all();
        return view('admin.vehicle_add',compact('makes'));
    }

    public function vehicle_submit(Request $request)
    {

        if(is_numeric($request->make)){
            $make = Make::findOrFail($request->make);
        }else{

            $make = new Make();

            $make->name = $request->make;
            $make->save();

        }

        if(is_numeric($request->model)){
            $model = Modal::findOrFail($request->model);
        }else{

            $model = new Modal();

            $model->name = $request->model;
            $model->make_idFk = $make->id;
            $model->save();
            //dd($model);
        }

        if(is_numeric($request->year)){
            $year = Year::findOrFail($request->year);
        }else{

            $year = new Year();

            $year->name = $request->year;
            $year->model_idFk = $model->id;
            $year->save();

        }

        if(is_numeric($request->wheel_base)){
            $wheelbase = WheelBase::findOrFail($request->wheel_base);
        }else{

            $wheelbase = new WheelBase();

            $wheelbase->name = $request->wheel_base;
            $wheelbase->year_idFk = $year->id;
            $wheelbase->abbreviation = $request->wb_abbreviation;
            $wheelbase->save();

        }

        if(is_numeric($request->roof_config)){
            $roof_configuration = RoofConfiguration::findOrFail($request->roof_config);
        }else{

            $roof_configuration = new RoofConfiguration();

            $roof_configuration->name = $request->roof_config;
            $roof_configuration->year_idFk =  $year->id;
            $roof_configuration->abbreviation =  $request->rc_abbreviation;
            $roof_configuration->save();

        }

        if(is_numeric($request->side_door)){
            $side_door = SideDoorConfiguration::findOrFail($request->side_door);
        }else{

            $side_door = new SideDoorConfiguration();

            $side_door->name = $request->side_door;
            $side_door->year_idFk =  $year->id;
            $side_door->abbreviation =  $request->sdc_abbreviation;
            $side_door->save();

        }

        if(is_numeric($request->rear_door)){
            $rear_doors = RearDoor::findOrFail($request->rear_door);
        }else{

            $rear_doors = new RearDoor();

            $rear_doors->name = $request->rear_door;
            $rear_doors->abbreviation =  $request->rdc_abbreviation;
            $rear_doors->year_idFk =  $year->id;
            $rear_doors->save();

        }

        if(OtherData::where('parking', $request->parking)->where('painted', $request->painted)->where('trim', $request->trim)->where('type', $request->type)->where('year_idFk', $year->id)->exists()){
            $other = OtherData::where('parking', $request->parking)->where('painted', $request->painted)->where('trim', $request->trim)->where('type', $request->type)->where('year_idFk', $year->id)->first();
        }else{

            $other = new OtherData();

            $other->year_idFk =  $year->id;
            $other->parking =  $request->parking;
            $other->painted =  $request->painted;
            $other->trim =  $request->trim;
            $other->type =  $request->type;
            $other->other =  $request->other;
            $other->save();

        }

        $row = new Row();

        $row->make      = $make->name;
        $row->model     = $model->name;
        $row->year      = $year->name;

        $row->parking   = $other->parking;
        $row->painted   = $other->painted;
        $row->type      = $other->type;
        $row->trim      = $other->trim;
        $row->other     = $other->other;

        $row->wheel_base            = $wheelbase->name;
        $row->wb_abbreviation       = $wheelbase->abbreviation;
        $row->roof_config           = $roof_configuration->name;
        $row->rc_abbreviation       = $roof_configuration->abbreviation;
        $row->side_door_config      = $side_door->name;
        $row->sdc_abbreviation      = $side_door->abbreviation;
        $row->rear_door_config      = $rear_doors->name;
        $row->rdc_abbreviation      = $rear_doors->abbreviation;

        $row->save();


        return redirect('admin/dashboard')->with('success', 'Vehicle Added!');
    }

    public function delete_all()
    {

        Row::truncate();

            $years = Year::all();
            if(count($years) > 0){
                foreach($years as $year){
                    OtherData::where('year_idFk', $year->id)->delete();
                    RearDoor::where('year_idFk', $year->id)->delete();
                    RoofConfiguration::where('year_idFk', $year->id)->delete();
                    WheelBase::where('year_idFk', $year->id)->delete();
                }
            }

            $models = Modal::all();
            if(count($models) > 0){
                foreach($models as $model){
                    Year::where('model_idFk', $model->id)->delete();
                }
            }

            $makes = Make::all();
            if(count($makes) > 0){
                foreach($makes as $make){
                    Modal::where('make_idFk', $make->id)->delete();
                    Make::where('id', $make->id)->delete();
                }
            }

        return redirect('admin/dashboard')->with('success', 'All Data Deleted!');

    }


}
