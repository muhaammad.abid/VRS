<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BonnetVariant;

class BonnetController extends Controller
{
    public function bonnetPage()
    {
    	$enduse = BonnetVariant::all();

    	return view('admin.setting.bonnet.list', compact('enduse'));
    }

    public function bonnetAdd()
    {

    	return view('admin.setting.bonnet.add');
    }

    public function bonnetSubmit(Request $request)
    {
    	$enduse = new BonnetVariant();

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/bonnet/list')->with('success', 'Added Successfully!');
    }

    public function bonnetEdit($id)
    {
    	$enduse = BonnetVariant::findOrFail($id);

    	return view('admin.setting.bonnet.edit', compact('enduse'));
    }

    public function bonnetDelete($id)
    {
    	$enduse = BonnetVariant::findOrFail($id);
    	$enduse->delete();
    	return redirect('admin/product/bonnet/list')->with('success', 'Deleted Successfully!');
    }

    public function bonnetUpdate(Request $request)
    {

    	$enduse = BonnetVariant::findOrFail($request->id);

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/bonnet/list')->with('success', 'Updated Successfully!');
    }
}
