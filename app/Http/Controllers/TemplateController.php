<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TemplateVariant;

class TemplateController extends Controller
{
    public function templatePage()
    {
    	$enduse = TemplateVariant::all();

    	return view('admin.setting.template.list', compact('enduse'));
    }

    public function templateAdd()
    {

    	return view('admin.setting.template.add');
    }

    public function templateSubmit(Request $request)
    {
    	$enduse = new TemplateVariant();

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/template/list')->with('success', 'Added Successfully!');
    }

    public function templateEdit($id)
    {
    	$enduse = TemplateVariant::findOrFail($id);

    	return view('admin.setting.template.edit', compact('enduse'));
    }

    public function templateDelete($id)
    {
    	$enduse = TemplateVariant::findOrFail($id);
    	$enduse->delete();
    	return redirect('admin/product/template/list')->with('success', 'Deleted Successfully!');
    }

    public function templateUpdate(Request $request)
    {

    	$enduse = TemplateVariant::findOrFail($request->id);

    	$enduse->name = $request->name;
    	$enduse->abbreviation = $request->abbreviation;

    	$enduse->save();

    	return redirect('admin/product/template/list')->with('success', 'Updated Successfully!');
    }
}
