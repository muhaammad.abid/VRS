<?php

namespace App\Http\Controllers;

use App\Row;
use Illuminate\Http\Request;
use App\User;
use App\Modal;
use App\OtherData;
use App\RearDoor;
use App\RoofConfiguration;
use App\SideDoorConfiguration;
use App\WheelBase;
use App\Year;
use Maatwebsite\Excel\Excel;

class ExportController extends Controller
{
    public function downloadExcel($type)
    {
        $rows =  Row::select('make', 'model', 'year','wheel_base','wb_abbreviation','roof_config','rc_abbreviation','side_door_config',
            'sdc_abbreviation','rear_door_config','rdc_abbreviation','parking','painted','trim','type','other')->get();

        $data = [];

        if(count($rows) > 0){

            foreach ($rows as $key => $value) {
                
                $data[$key]['make'] = $value->make;
                $data[$key]['model'] = $value->model;
                $data[$key]['generation_year'] = $value->year;
                $data[$key]['wheel_base'] = $value->wheel_base;
                $data[$key]['wb_abbreviation'] = $value->wb_abbreviation;
                $data[$key]['roof_configuration'] = $value->roof_config;
                $data[$key]['rc_abbreviation'] = $value->rc_abbreviation;
                $data[$key]['side_door_config'] = $value->side_door_config;
                $data[$key]['sdc_abbreviation'] = $value->sdc_abbreviation;
                $data[$key]['rear_doors_config'] = $value->rear_door_config;
                $data[$key]['rdc_abbreviation'] = $value->rdc_abbreviation;
                $data[$key]['parking_sensors_present'] = $value->parking;
                $data[$key]['painted_bumpers'] = $value->painted;
                $data[$key]['variant_trim'] = $value->trim;
                $data[$key]['vehice_type'] = $value->type;
                $data[$key]['other_info'] = $value->other;

            }

        }else{
            return back()->with('error', 'Record Not Found!');
        }

        $name = date('Y-m-d H:i').' Vehicles';

        \Excel::create($name, function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);

    }
}
