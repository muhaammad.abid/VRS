<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WheelBase extends Model
{
    public function year()
    {
        return $this->belongsTo('App\Year', 'year_idFk');
    }
}
