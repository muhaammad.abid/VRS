<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    public function modal()
    {
        return $this->belongsTo('App\Modal', 'model_idFk');
    }

    public function others()
    {
        return $this->hasMany('App\OtherData', 'year_idFk');
    }
    public function rearDoor()
    {
        return $this->hasMany('App\RearDoor', 'year_idFk');
    }
    public function sideDoor()
    {
        return $this->hasMany('App\SideDoor', 'year_idFk');
    }
    public function roofConfig()
    {
        return $this->hasMany('App\RoofConfiguration', 'year_idFk');
    }
    public function wheelBase()
    {
        return $this->hasMany('App\WheelBase', 'year_idFk');
    }
}
