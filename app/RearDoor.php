<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RearDoor extends Model
{
    public function sideDoor(){
        return $this->belongsTo('App\SideDoorConfiguration', 'side_idFk');
    }
}
