<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modal extends Model
{
    protected $table = 'models';

    public function make()
    {
        return $this->belongsTo('App\Make', 'make_idFk');
    }
}
