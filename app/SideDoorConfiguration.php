<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SideDoorConfiguration extends Model
{
    public function roofConfig(){
        return $this->belongsTo('App\RoofConfiguration', 'roof_idFk');
    }
}
