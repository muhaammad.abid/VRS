/*Form advanced Init*/
$(document).ready(function() {
"use strict";

/* Select2 Init*/
$(".select2").select2();



/* Bootstrap switch Init*/
$('.bs-switch').bootstrapSwitch('state', false);
$('#check_box_value').text($("#check_box_switch").bootstrapSwitch('state'));

$('#check_box_switch').on('switchChange.bootstrapSwitch', function () {

	$("#check_box_value").text($('#check_box_switch').bootstrapSwitch('state'));
});

});