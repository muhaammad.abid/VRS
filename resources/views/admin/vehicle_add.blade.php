@extends('admin.layout')
@section('title', 'Add Vehicle')
@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class=" full-width full-height">
            <div class=" vertical-align-middle auth-form-wrap">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="mb-30">
                                <!-- <h3 class="text-center txt-dark mb-10">Sign up to Doodle</h3>
                                    <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6> -->
                            </div>
                            <div class="form-wrap">
                                <form action="{{url('vehicle/submit')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <h5>Vehicle Information</h5>
                                                <hr>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label class="control-label mb-10">Make</label>
                                                    <select class="select2 tagS" multiple="multiple" data-placeholder="Choose" name="make" id="check1" required>
                                                        @foreach($makes as $make)
                                                            <option value="{{$make->id}}">{{$make->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group  col-md-6">
                                                    <label class="control-label mb-10">Model</label>
                                                    <select class="select2 tagS" multiple="multiple" data-placeholder="Choose" name="model" id="check2" required>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label class="control-label mb-10">Generation / [Year]</label>
                                                    <select class="select2 tagS" multiple="multiple" data-placeholder="Choose" name="year" id="check3" required>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="control-label mb-10">Wheel Base</label>
                                                    <select class="select2 tagS" multiple="multiple" data-placeholder="Choose" name="wheel_base" data-id="5" id="check4" required>
                                                    </select>
                                                    <!-- <select class="select2 tagS" multiple="multiple" data-placeholder="Choose" name="wb_abbreviation" style="display: none;">
                                                    </select> -->
                                                    <select name="wb_abbreviation" multiple="multiple" class="form-control abbreviationSelect2" data-id="5" data-placeholder="abbreviation" style="display: none;"></select>
                                                </div>
                                            </div>
                                           <div class="row">
                                               <div class="form-group col-md-6">
                                                   <label class="control-label mb-10">Roof Configuration</label>
                                                   <select class="select2 tagS" multiple="multiple" data-placeholder="Choose" name="roof_config" data-id="6" id="check5" required>
                                                   </select>
                                                   <select name="rc_abbreviation" multiple="multiple" class="form-control abbreviationSelect2" data-id="6" data-placeholder="abbreviation" style="display: none;"></select>
                                               </div>
                                               <div class="form-group col-md-6">
                                                   <label class="control-label mb-10">Side Door Configuration</label>
                                                   <select class="select2 tagS" multiple="multiple" data-placeholder="Choose" name="side_door" data-id="7" id="check6" required>
                                                   </select>
                                                   <select name="sdc_abbreviation" multiple="multiple" class="form-control abbreviationSelect2" data-id="7" data-placeholder="abbreviation" style="display: none;"></select>
                                               </div>
                                           </div>
                                           <div class="row">
                                               <div class="form-group col-md-6">
                                                   <label class="control-label mb-10">Rear Door Configuration</label>
                                                   <select class="select2 tagS" multiple="multiple" data-placeholder="Choose" name="rear_door" data-id="8" id="check7" required>
                                                   </select>
                                                   <select name="rdc_abbreviation" multiple="multiple" class="form-control abbreviationSelect2" data-id="8" data-placeholder="abbreviation" style="display: none;"></select>
                                               </div>
                                               <div class="form-group col-md-6">
                                                   <label class="control-label mb-10">Parking Sensors</label>
                                                   <select class="select2 tagS otherSelect2" data-id="9" multiple="multiple" data-placeholder="Choose" name="parking" id="check8" required>
                                                   </select>

                                               </div>
                                           </div>
                                           <div class="row">
                                               <div class="form-group col-md-6">
                                                   <label class="control-label mb-10">Painted Bumpers</label>
                                                   <select class="select2 tagS otherSelect2" data-id="10" multiple="multiple" data-placeholder="Choose" name="painted" id="check9" required>
                                                   </select>
                                               </div>
                                               <div class="form-group col-md-6">
                                                   <label class="control-label mb-10">Variant / Trim</label>
                                                   <select class="select2 tagS otherSelect2" data-id="11" multiple="multiple" data-placeholder="Choose" name="trim" id="check10" required >
                                                   </select>
                                               </div>
                                           </div>
                                           <div class="row">
                                               <div class="form-group col-md-6">
                                                   <label class="control-label mb-10">Vehicle Type</label>
                                                   <select class="select2 tagS otherSelect2" data-id="12" multiple="multiple" data-placeholder="Choose" name="type" id="check11" required>
                                                   </select>
                                               </div>
                                               <div class="form-group col-md-6">
                                                   <label class="control-label mb-10">Other Info</label>
                                                   <select class="select2 tagS otherSelect2" multiple="multiple" data-placeholder="Choose" name="other" id="check12" required>
                                                   </select>
                                               </div>
                                           </div>
                                        </div>

                                    </div>


                                    <br>
                                    <br>
                                    <hr>
                                    <div class="form-group text-center">

                                        <button type="submit" class="btn btn-info btn-rounded">Add Vehicle</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>

    <!--  <a href="#!" data-toggle="modal" data-target="#myModal">Modal</a> -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="background: transparent;box-shadow: none;text-align: center;">
                <form action="{{url('importExcel')}}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <img src="{{url('loading.gif')}}">
                </div>
                </form>
            </div>
        </div>
    </div>

@stop

@section('foot')
    @parent
    <script>
        $(document).ready(function() {

            $(".tagS").select2({
                tags: true,
                maximumSelectionLength: 1
            });

            $('#check1').on('change', function () {
                var id = $(this).val();

                if(id){
                  $('#myModal').modal('show');
                    $.ajax({  
                        method: 'GET',
                        url: '{{ url('admin/model') }}/' + id,
                        dataType: '',
                        success: function (model) {

                            $('#check2').html(" ");
                            $('#check2').select2({data: ''}); 
                            $('#check2').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });

                            for (i = 0; i < model.length; i++) {
                                $('#check2').append("<option value='" + model[i]["id"] + "'>" + model[i]["name"] + "</option>");

                            }

                            $('#check2').closest('.form-group').find('.select2-search__field').focus();
                        }
                    });


                    $('#myModal').modal('hide');
                }
            });

            $('#check2').on('change', function () {
                var id = $(this).val();
                if(id){
                  $('#myModal').modal('show');
                    $.ajax({
                        method: 'GET',
                        url: '{{ url('admin/year') }}/' + id,
                        dataType: '',
                        success: function (model) {

                            $('#check3').html(" ");
                            $('#check3').select2({data: ''}); 
                            $('#check3').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });

                            for (i = 0; i < model.length; i++) {
                                $('#check3').append("<option value='" + model[i]["id"] + "'>" + model[i]["name"] + "</option>");

                            }

                            $('#check3').closest('.form-group').find('.select2-search__field').focus();
                        }
                    });
                    $('#myModal').modal('hide');
                }
            });
            $('#check3').on('change', function () {
                var id = $(this).val();
                if(id){
                  $('#myModal').modal('show');
                    $.ajax({
                        method: 'GET',
                        url: '{{ url('admin/all') }}/'+id,
                        success: function (all) {

                            $('#check4').html(" ");
                            $('#check5').html(" ");
                            $('#check6').html(" ");
                            $('#check7').html(" ");
                            $('#check8').html(" ");
                            $('#check9').html(" ");
                            $('#check10').html(" ");
                            $('#check11').html(" ");
                            $('#check12').html(" ");

                            $('#check4').select2({data: ''});
                            $('#check5').select2({data: ''});
                            $('#check6').select2({data: ''});
                            $('#check7').select2({data: ''});
                            $('#check8').select2({data: ''});
                            $('#check9').select2({data: ''});
                            $('#check10').select2({data: ''});
                            $('#check11').select2({data: ''});
                            $('#check12').select2({data: ''});

                            $('#check4').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });
                            $('#check5').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });
                            $('#check6').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });
                            $('#check7').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });
                            $('#check8').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });
                            $('#check9').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });
                            $('#check10').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });
                            $('#check11').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });
                            $('#check12').select2({
                                tags: true,
                                maximumSelectionLength: 1
                            });
                            for(i = 0; i < all.wheel_base.length; i++){

                                $('#check4').append("<option value='"+all.wheel_base[i]["id"]+"'>"+all.wheel_base[i]["name"]+' (' + all.wheel_base[i]["abbreviation"]+')'+"</option>");
                            }

                            for(i = 0; i < all.roof_config.length; i++){
                                $('#check5').append("<option value='"+all.roof_config[i]["id"]+"'>"+all.roof_config[i]["name"]+' (' + all.roof_config[i]["abbreviation"]+')'+"</option>");
                            }

                            for(i = 0; i < all.side_door_config.length; i++){
                                $('#check6').append("<option value='"+all.side_door_config[i]["id"]+"'>"+all.side_door_config[i]["name"]+' (' + all.side_door_config[i]["abbreviation"]+')'+"</option>");
                            }

                            for(i = 0; i < all.rear_door.length; i++){
                                $('#check7').append("<option value='"+all.rear_door[i]["id"]+"'>"+all.rear_door[i]["name"]+' (' + all.rear_door[i]["abbreviation"]+')'+"</option>");
                            }
                            for(i = 0; i < all.other_data.length; i++){
                                $('#check8').append("<option value='"+all.other_data[i]["parking"]+"'>"+all.other_data[i]["parking"]+"</option>");
                                $('#check9').append("<option value='"+all.other_data[i]["painted"]+"'>"+all.other_data[i]["painted"]+"</option>");
                                $('#check10').append("<option value='"+all.other_data[i]["trim"]+"'>"+all.other_data[i]["trim"]+"</option>");
                                $('#check11').append("<option value='"+all.other_data[i]["type"]+"'>"+all.other_data[i]["type"]+"</option>");
                                $('#check12').append("<option value='"+all.other_data[i]["other"]+"'>"+all.other_data[i]["other"]+"</option>");
                            }

                            $('#check4').closest('.form-group').find('.select2-search__field').focus();
                        }
                    });
                    $('#myModal').modal('hide');
                }
            });

            $('#check4, #check5, #check6, #check7').on('change', function(){
                var value = $(this).val();
                var id = $(this).attr('id');
                if(isNaN(value)){
                    $(this).closest('.form-group').find('.form-control').show();
                    $(this).closest('.form-group').find('.form-control').select2({
                      tags: true,
                      maximumSelectionLength: 1
                    });

                    $.ajax({
                      method: 'GET',
                      url: '{{ url("admin/id/") }}/'+id,
                      success: function (id) {
                        $('#'+id).closest('.form-group').find('.select2-search__field').last().focus();
                      }

                    });

                    $(this).closest('.form-group').find('.select2').last().find('.select2-search__field').focus();

                    $(this).closest('.form-group').find('.form-control').attr('required', true);
                }else{
                    if(value){
                      $(this).closest('.form-group').find('.form-control').hide();
                      $(this).closest('.form-group').find('.form-control').attr('required', false);
                      var id = $(this).data('id');
                      $.ajax({
                        method: 'GET',
                        url: '{{ url("admin/id/") }}/'+id,
                        success: function (id) {
                          $('#check'+id).closest('.form-group').find('.select2-search__field').first().focus();
                        }

                      });
                    }
                }
            });

            $('.abbreviationSelect2').on('change', function(e){
              var id = $(this).data('id');
              if(id){
                $.ajax({
                  method: 'GET',
                  url: '{{ url("admin/id/") }}/'+id,
                  success: function (id) {
                    $('#check'+id).closest('.form-group').find('.select2-search__field').focus();
                  }

                });
              }

            });

            $('.otherSelect2').on('change', function(e){
              var id = $(this).data('id');
               if(id){
                $.ajax({
                    method: 'GET',
                    url: '{{ url("admin/id/") }}/'+id,
                    success: function (id) {
                      $('#check'+id).closest('.form-group').find('.select2-search__field').focus();
                    }

                  });
               }


            });


        });

    </script>

@endsection