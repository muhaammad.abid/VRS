@extends('admin.layout')
@section('title', 'Dashboard')
@section('content')
    <div class="page-wrapper" style="margin-left:0px;">
        <div class="container-fluid" style="margin-top: 65px;">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Welcome to <code>Vehicle Livery Solutions</code></h6>
                            </div>

                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('vehicle_add')}}" class="btn btn-info btn-rounded btn-block btn-anim">
                                        <i class="fa fa-plus"></i>
                                        <span class="btn-text">Add New Vehicle</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="#" class="btn btn-primary btn-rounded btn-block btn-anim" data-toggle="modal" data-target="#myModal">
                                        <i class="fa fa-cloud-upload"></i>
                                        <span class="btn-text">Import</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('downloadExcel/'.'csv')}}" class="btn btn-success btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cloud-download"></i>
                                        <span class="btn-text">Export</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/results')}}" class="btn btn-default btn-rounded btn-block btn-anim">
                                        <i class="fa fa-list"></i>
                                        <span class="btn-text">Results</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/product/settings')}}" class="btn btn-primary btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Product Settings</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('changepassword')}}" class="btn btn-warning btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Change Password</span>
                                    </a>
                                </div>


                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Import CSV</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{url('importExcel')}}" method="post" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="file-loading">
                                            <input id="input-b9" name="import_file" type="file">
                                        </div>
                                        <!-- <div class="checkbox">
                                            <label><input type="checkbox" name="clear" value="Yes">Clear Oldest</label>
                                        </div> -->
                                        <label class="radio-inline"><input type="radio" value="replace" name="clear">Replace All</label>
                                        <label class="radio-inline"><input type="radio" value="udpate" checked="" name="clear">Update</label>
                                        <div id="kartik-file-errors"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary" title="Your custom upload logic">Upload</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Vehicles</h6>
                            </div>
                            <div class="pull-right">
                                <a href="{{url('admin/vehicles/delete/all')}}" class="btn btn-warning">Delete All</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table @if(count($rows) > 0) id="datable_1" @endif class="table table-hover display  pb-30">
                                            <thead>
                                            <tr>
                                                <th>Make</th>
                                                <th>Model</th>
                                                <th>year</th>
                                                <th>whl Base</th>
                                                <th>wb Abb</th>
                                                <th>Roof Conf</th>
                                                <th>RC Abb</th>
                                                <th>SD Config</th>
                                                <th>SDC Abb</th>
                                                <th>RD Config</th>
                                                <th>RDC Abb</th>
                                                <th>Parking</th>
                                                <th>Painted</th>
                                                <th>Trim</th>
                                                <th>Vcl Type</th>
                                                <th>Other</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>Make</th>
                                                <th>Model</th>
                                                <th>year</th>
                                                <th>whl Base</th>
                                                <th>wb Abb</th>
                                                <th>Roof Conf</th>
                                                <th>RC Abb</th>
                                                <th>SD Config</th>
                                                <th>SDC Abb</th>
                                                <th>RD Config</th>
                                                <th>RDC Abb</th>
                                                <th>Parking</th>
                                                <th>Painted</th>
                                                <th>Trim</th>
                                                <th>Vcl Type</th>
                                                <th>Other</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            @if(count($rows) > 0)

                                                @foreach($rows as $row)
                                                    <tr>
                                                        <td>{{$row->make}}</td>
                                                        <td>{{$row->model}}</td>
                                                        <td>{{$row->year}}</td>
                                                        <td>{{$row->wheel_base}}</td>
                                                        <td>{{$row->wb_abbreviation}}</td>
                                                        <td>{{$row->roof_config}}</td>
                                                        <td>{{$row->rc_abbreviation}}</td>
                                                        <td>{{$row->side_door_config}}</td>
                                                        <td>{{$row->sdc_abbreviation}}</td>
                                                        <td>{{$row->rear_door_config}}</td>
                                                        <td>{{$row->rdc_abbreviation}}</td>
                                                        <td>{{$row->parking}}</td>
                                                        <td>{{$row->painted}}</td>
                                                        <td>{{$row->trim}}</td>
                                                        <td>{{$row->type}}</td>
                                                        <td>{{$row->other}}</td>
                                                    </tr>
                                                @endforeach

                                                @else

                                                <tr>
                                                    <td colspan="16">Record Not Found</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>
    </div>
@stop

@section('foot')
    @parent
    <script src="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js')}}"></script>
    <script>
        $(document).on('ready', function() {
            $("#input-b9").fileinput({
                showPreview: false,
                showUpload: false,
                elErrorContainer: '#kartik-file-errors',
                allowedFileExtensions: ["csv"]
                //uploadUrl: '/site/file-upload-single'
            });
        });
    </script>
@endsection