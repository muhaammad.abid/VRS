@extends('admin.layout')
@section('title', 'Edit')
@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle auth-form-wrap">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="mb-30">
                                <!-- <h3 class="text-center txt-dark mb-10">Sign up to Doodle</h3>
                                    <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6> -->
                            </div>
                            <div class="form-wrap">
                                <form action="{{url('admin/product/batten/update')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="id" value="{{$batten->id}}">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <h5>Battenberg Kits</h5>
                                                <hr>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label class="control-label mb-10">Name</label>
                                                    <input type="text" name="name" value="{{$batten->name}}" required="" class="form-control">
                                                </div>
                                                <div class="form-group  col-md-6">
                                                    <label class="control-label mb-10">Abbreviation</label>
                                                    <input type="text" name="abbreviation"  value="{{$batten->abbreviation}}" required="" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <br>
                                    <br>
                                    <hr>
                                    <div class="form-group text-right">
                                        <a href="{{url('admin/product/settings')}}" class="btn btn-default btn-rounded">Back</a>
                                        <button type="submit" class="btn btn-info btn-rounded">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>


@stop

@section('foot')
    @parent
    <script>
        $(document).ready(function() {


        });

    </script>

@endsection