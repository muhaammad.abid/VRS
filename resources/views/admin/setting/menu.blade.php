<div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/product/settings')}}" class="btn btn-info btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Battenberg</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a class="btn btn-primary btn-rounded btn-block btn-anim" href="{{url('admin/product/enduse/list')}}">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">End Use</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/product/chevron/list')}}" class="btn btn-success btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Chevron Kit</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/product/escort/list')}}" class="btn btn-default btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Escort Vehicle</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/product/bonnet/list')}}" class="btn btn-primary btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Bonnet Marketing</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/product/template/list')}}" class="btn btn-warning btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Templates</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 col-md-offset-4 mt-15">
                                    <a href="{{url('admin/product/grade/list')}}" class="btn btn-default btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Grade</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/product/material/list')}}" class="btn btn-warning btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Materials</span>
                                    </a>
                                </div>

                                <!-- <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/product/other/list')}}" class="btn btn-success btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cloud-download"></i>
                                        <span class="btn-text">Other Properties</span>
                                    </a>
                                </div> -->

                                