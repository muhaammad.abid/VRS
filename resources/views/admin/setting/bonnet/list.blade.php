@extends('admin.layout')
@section('title', 'Bonnet Marketing')
@section('content')
    <div class="page-wrapper" style="margin-left:0px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Welcome to <code>Vehicle Livery Solutions</code></h6>
                            </div>

                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">

                                <!-- menu bar -->
                                @include('admin.setting.menu')
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            @if(session('success'))
                                    <p class="alert alert-success">{{session('success')}}</p>
                                @endif
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Bonnet Marketing</h6>
                            </div>
                            <div class="pull-right">
                                <a href="{{url('admin/product/bonnet/add')}}" class="btn btn-info">Add</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table @if(count($enduse) > 0) id="datable_1" @endif class="table table-hover display  pb-30">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Abbreviation</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Abbreviation</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            @if(count($enduse) > 0)

                                                @foreach($enduse as $bat)
                                                    <tr>
                                                        <td>{{$bat->id}}</td>
                                                        <td>{{$bat->name}}</td>
                                                        <td>{{$bat->abbreviation}}</td>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <a href="{{url('admin/product/bonnet/edit/'.$bat->id)}}" class="btn btn-info">Edit</a>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <a href="{{url('admin/product/bonnet/delete/'.$bat->id)}}" class="btn btn-danger">Delete</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                @else

                                                <tr>
                                                    <td colspan="16">Record Not Found</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>
    </div>
@stop

@section('foot')
    @parent
    <script src="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js')}}"></script>
    <script>
        $(document).on('ready', function() {
            $("#input-b9").fileinput({
                showPreview: false,
                showUpload: false,
                elErrorContainer: '#kartik-file-errors',
                allowedFileExtensions: ["csv", "xls"]
                //uploadUrl: '/site/file-upload-single'
            });
        });
    </script>
@endsection