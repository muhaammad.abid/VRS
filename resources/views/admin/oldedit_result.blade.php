@extends('admin.layout')

@section('title', 'Edit Result')

@section('content')


    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <!-- <h3 class="text-center txt-dark mb-10">Sign up to Doodle</h3>
                                        <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6> -->
                                </div>
                                <div class="form-wrap">
                                    <form action="{{ url('form_data/update') }}" method="GET">
                                        <input type="hidden" name="brand" value="{{ $result->brand}}">
                                        <input type="hidden" name="result_id" value="{{$result->id}}">
                                        <div class="row">
                                            <div class="col-lg-12" style="margin-bottom: 25px;">

                                                <h3 style="text-align: center;">
                                                    @if($result->brand == 1)
                                                        Battenberg Kits
                                                    @elseif($result->brand == 2)
                                                        Chevron Kit
                                                    @elseif($result->brand == 3)
                                                        Escort Vehicle
                                                    @elseif($result->brand == 4)
                                                        Side Chevrons
                                                    @elseif($result->brand == 5)
                                                        Bonnet Markings
                                                    @elseif($result->brand == 6)
                                                        Templates
                                                    @endif
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <h5>Vehicle Information</h5>
                                                    <hr>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label mb-10">Make</label>
                                                    <select required="" id="check1" class="form-control select2" name="make">
                                                        <option selected disabled value="" >Select an option</option>
                                                        @foreach($makes as $make)
                                                            <option value="{{$make->id}}" @if($result->make == $make->id) selected @endif>{{$make->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label mb-10">Model</label>
                                                    <select required="" id="check2" class="form-control select2" name="model">
                                                        <option selected disabled value="" >Select an option</option>
                                                        @foreach($models as $model)
                                                            <option value="{{$model->id}}" @if($model->id == $result->model) selected @endif>{{$model->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label mb-10">Generation / [Year]</label>
                                                    <select required="" id="check3" class="form-control select2" name="year">
                                                        <option selected disabled value="" >Select an option</option>
                                                        @foreach($years as $year)
                                                            <option value="{{$year->id}}" @if($year->id == $result->year) selected @endif>{{$year->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                               @if($result->brand == 1)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Wheel Base</label>
                                                        <select required="" id="check4" class="form-control select2" name="wheel_base">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($wheel_base as $wheel)
                                                                <option value="{{$wheel->id}}" @if($wheel->id == $result->wheel_base) selected @endif>{{$wheel->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Roof Configuration</label>
                                                        <select required="" id="check5" class="form-control select2" name="roof_config">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($roof_config as $roof)
                                                                <option value="{{$roof->id}}" @if($roof->id == $result->roof_config) selected @endif>{{$roof->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Side Door Configuration</label>
                                                        <select required="" id="check6" class="form-control select2" name="side_door_config">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($side_door_config as $side)
                                                                <option value="{{$side->id}}" @if($side->id == $result->side_door_config) selected @endif>{{$side->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Rear Door Configuration</label>
                                                        <select required="" id="check7" class="form-control select2" name="rear_door">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($rear_door as $rear)
                                                                <option value="{{$rear->id}}" @if($rear->id == $result->rear_door) selected @endif>{{$rear->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label class="control-label mb-10">Parking Sensors</label>
                                                        <select required="" id="check8" class="form-control select2" name="parking">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option value="{{$result->parking}}" selected="">{{$result->parking}}</option>
                                                        </select>
                                                    </div> -->
                                               @elseif($result->brand == 2)

                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Roof Configuration</label>
                                                        <select required="" id="check5" class="form-control select2" name="roof_config">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($roof_config as $roof)
                                                                <option value="{{$roof->id}}" @if($roof->id == $result->roof_config) selected @endif>{{$roof->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Rear Door Configuration</label>
                                                        <select required="" id="check7" class="form-control select2" name="rear_door">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($rear_door as $rear)
                                                                <option value="{{$rear->id}}" @if($rear->id == $result->rear_door) selected @endif>{{$rear->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                @elseif($result->brand == 3)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Wheel Base</label>
                                                        <select required="" id="check4" class="form-control select2" name="wheel_base">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($wheel_base as $wheel)
                                                                <option value="{{$wheel->id}}" @if($wheel->id == $result->wheel_base) selected @endif>{{$wheel->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Roof Configuration</label>
                                                        <select required="" id="check5" class="form-control select2" name="roof_config">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($roof_config as $roof)
                                                                <option value="{{$roof->id}}" @if($roof->id == $result->roof_config) selected @endif>{{$roof->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Side Door Configuration</label>
                                                        <select required="" id="check6" class="form-control select2" name="side_door_config">
                                                            <option selected disabled value="">Select an option</option>
                                                             @foreach($side_door_config as $side)
                                                                <option value="{{$side->id}}" @if($side->id == $result->side_door_config) selected @endif>{{$side->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Rear Door Configuration</label>
                                                        <select required="" id="check7" class="form-control select2" name="rear_door">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($rear_door as $rear)
                                                                <option value="{{$rear->id}}" @if($rear->id == $result->rear_door) selected @endif>{{$rear->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                @elseif($result->brand == 4)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Wheel Base</label>
                                                        <select required="" id="check4" class="form-control select2" name="wheel_base">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                @elseif($result->brand == 5)

                                                @elseif($result->brand == 6)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Wheel Base</label>
                                                        <select required="" id="check4" class="form-control select2" name="wheel_base">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($wheel_base as $wheel)
                                                                <option value="{{$wheel->id}}" @if($wheel->id == $result->wheel_base) selected @endif>{{$wheel->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Roof Configuration</label>
                                                        <select required="" id="check5" class="form-control select2" name="roof_config">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($roof_config as $roof)
                                                                <option value="{{$roof->id}}" @if($roof->id == $result->roof_config) selected @endif>{{$roof->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Side Door Configuration</label>
                                                        <select required="" id="check6" class="form-control select2" name="side_door_config">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($side_door_config as $side)
                                                                <option value="{{$side->id}}" @if($side->id == $result->side_door_config) selected @endif>{{$side->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Rear Door Configuration</label>
                                                        <select required="" id="check7" class="form-control select2" name="rear_door">
                                                            <option selected disabled value="">Select an option</option>
                                                            @foreach($rear_door as $rear)
                                                                <option value="{{$rear->id}}" @if($rear->id == $result->rear_door) selected @endif>{{$rear->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <h5>Product Information</h5>
                                                    <hr>
                                                </div>
                                                @if($result->brand == 1)
                                                    
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Battenburg Variant</label>
                                                        <select required="" id="check9" class="form-control select2" name="variant">
                                                            <option selected disabled value="">Select an option</option>
                                                            @if(count($battens) > 0)
                                                                @foreach($battens as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}" @if(strpos($result->variant, $bat->name) !== false) selected @endif>{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                            <option  value="Full Battenburg (FB">Full Battenburg (FB)</option>
                                                            <option @if(strpos($result->variant, '3-Quarter Battenburg') !== false) selected @endif value="3-Quarter Battenburg (3Q">3-Quarter Battenburg (3Q)</option>
                                                            <option @if(strpos($result->variant, '3-Quarter With White Lowers') !== false) selected @endif value="3-Quarter With White Lowers (3W">3-Quarter With White Lowers (3W)</option>
                                                            <option @if(strpos($result->variant, 'Half Battenburg') !== false) selected @endif value="Half Battenburg (HB">Half Battenburg (HB)</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Material</label>
                                                        <select required="" id="check10" class="form-control select2" name="material">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option @if(strpos($result->material, '3M Diamond Grade') !== false) selected @endif value="3M Diamond Grade (3MD">3M Diamond Grade (3MD)</option>
                                                            <option @if(strpos($result->material, 'Reflexite VC612') !== false) selected @endif value="Reflexite VC612 (612">Reflexite VC612 (612)</option>
                                                            <option @if(strpos($result->material, 'Oralite 6900') !== false) selected @endif value="Oralite 6900 (690">Oralite 6900 (690)</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label mb-10">End Use</label>
                                                        <select required="" id="check11" class="form-control select2" name="end_user">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option @if(strpos($result->end_use, 'Police Forces') !== false) selected @endif value="Police Forces (PF">Police Forces (PF)</option>
                                                            <option @if(strpos($result->end_use, 'Ambulance and Doctors') !== false) selected @endif value="Ambulance and Doctors (AM">Ambulance and Doctors (AM)</option>
                                                            <option @if(strpos($result->end_use, 'Fire and Rescue') !== false) selected @endif value="Fire and Rescue (FR">Fire and Rescue (FR)</option>
                                                            <option @if(strpos($result->end_use, 'NHS Blood and Translate') !== false) selected @endif value="NHS Blood and Translate, Blood Bikes (NB">NHS Blood and Translate, Blood Bikes (NB)</option>
                                                            <option @if(strpos($result->end_use, 'Highways England and VOSA') !== false) selected @endif value="Highways England and VOSA (HE">Highways England and VOSA (HE)</option>
                                                            <option @if(strpos($result->end_use, 'Rail Response') !== false) selected @endif value="Rail Response (RR">Rail Response (RR)</option>
                                                            <option @if(strpos($result->end_use, 'Mountain Rescue') !== false) selected @endif value="Mountain Rescue (MR">Mountain Rescue (MR)</option>
                                                            <option @if(strpos($result->end_use, 'HM Coastguard') !== false) selected @endif value="HM Coastguard (HC">HM Coastguard (HC)</option>
                                                            <option @if(strpos($result->end_use, 'Immigration Enforcement') !== false) selected @endif value="Immigration Enforcement (IE">Immigration Enforcement (IE)</option>
                                                            <option @if(strpos($result->end_use, 'Highways England Contractors') !== false) selected @endif value="Highways England Contractors (HC">Highways England Contractors (HC)</option>
                                                        </select>
                                                    </div>
                                                @elseif($result->brand == 2)

                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Chevron Kit Variant</label>
                                                        <select required="" id="check9" class="form-control select2" name="variant">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option @if(strpos($result->variant, 'Full Chevron') !== false) selected @endif value="Full Chevron">Full Chevron</option>
                                                            <option @if(strpos($result->variant, 'Half Chevron') !== false) selected @endif value="Half Chevron">Half Chevron</option>
                                                            <option @if(strpos($result->variant, '3-Quarter Kit Chevron') !== false) selected @endif value="3-Quarter Kit Chevron">3-Quarter Kit Chevron</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Grade</label>
                                                        <select required="" id="check10" class="form-control select2" name="grade">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option @if(strpos($result->grade, 'Entry Grade') !== false) selected @endif value="Entry Grade">Entry Grade</option>
                                                            <option @if(strpos($result->grade, 'Intermediate Grade') !== false) selected @endif value="Intermediate Grade">Intermediate Grade</option>
                                                            <option @if(strpos($result->grade, 'Premium Grade') !== false) selected @endif value="Premium Grade">Premium Grade</option>
                                                        </select>
                                                    </div>

                                                @elseif($result->brand == 3)
                                                     <div class="form-group">
                                                        <label class="control-label mb-10">Escprt Vehicle Variant</label>
                                                        <select required="" id="check9" class="form-control select2" name="variant">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option @if(strpos($result->variant, '150mm Chevrons') !== false) selected @endif  value="150mm Chevrons (150">150mm Chevrons (150)</option>
                                                            <option @if(strpos($result->variant, '250mm Chevrons') !== false) selected @endif  value="250mm Chevrons (250">250mm Chevrons (250)</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Material</label>
                                                        <select required="" id="check10" class="form-control select2" name="material">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option @if(strpos($result->material, '3M Diamond Grade') !== false) selected @endif value="3M Diamond Grade (3MD">3M Diamond Grade (3MD)</option>
                                                            <option @if(strpos($result->material, 'Reflexite VC612') !== false) selected @endif value="Reflexite VC612 (612">Reflexite VC612 (612)</option>
                                                            <option @if(strpos($result->material, 'Oralite 6900') !== false) selected @endif value="Oralite 6900 (690">Oralite 6900 (690)</option>
                                                        </select>
                                                    </div>
                                                   <!--  <div class="form-group">
                                                        <label class="control-label mb-10">End Use</label>
                                                        <select required="" id="check11" class="form-control select2">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option value="2">Police Forces</option>
                                                            <option value="3">Ambulance and Doctors</option>
                                                            <option value="4">Fire and Rescue</option>
                                                        </select>
                                                    </div> -->
                                                @elseif($result->brand == 4)

                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Grade</label>
                                                        <select required="" id="check10" class="form-control select2" name="grade">
                                                            <option selected disabled value="">Select an option</option>
                                                             <option @if(strpos($result->grade, 'Entry Grade') !== false) selected @endif value="Entry Grade">Entry Grade</option>
                                                            <option @if(strpos($result->grade, 'Intermediate Grade') !== false) selected @endif value="Intermediate Grade">Intermediate Grade</option>
                                                            <option @if(strpos($result->grade, 'Premium Grade') !== false) selected @endif value="Premium Grade">Premium Grade</option>
                                                        </select>
                                                    </div>
                                                @elseif($result->brand == 5)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Bonnet Marking Variants</label>
                                                        <select required="" id="check9" class="form-control select2" name="variant">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option @if(strpos($result->variant, 'Bonnet Chevrons') !== false) selected @endif  value="Bonnet Chevrons (BC">Bonnet Chevrons (BC)</option>
                                                            <option @if(strpos($result->variant, 'Bonnet Wrap') !== false) selected @endif  value="Bonnet Wrap (BW">Bonnet Wrap (BW)</option>
                                                            <option @if(strpos($result->variant, 'Bonnet Flutes') !== false) selected @endif  value="Bonnet Flutes (BF">Bonnet Flutes (BF)</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Grade</label>
                                                        <select required="" id="check10" class="form-control select2" name="grade">
                                                            <option selected disabled value="">Select an option</option>
                                                             <option @if(strpos($result->grade, 'Entry Grade') !== false) selected @endif value="Entry Grade">Entry Grade</option>
                                                            <option @if(strpos($result->grade, 'Intermediate Grade') !== false) selected @endif value="Intermediate Grade">Intermediate Grade</option>
                                                            <option @if(strpos($result->grade, 'Premium Grade') !== false) selected @endif value="Premium Grade">Premium Grade</option>
                                                        </select>
                                                    </div>

                                                @else
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Templates Variant</label>
                                                        <select required="" id="check9" class="form-control select2" name="variant">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option @if(strpos($result->variant, 'Emergency Service Vehicle 6mm') !== false) selected @endif value="Emergency Service Vehicle 6mm (ESV">Emergency Service Vehicle 6mm (ESV)</option>
                                                            <option @if(strpos($result->variant, 'Escort Vehicle 6mm') !== false) selected @endif value="Escort Vehicle 6mm (ES">Escort Vehicle 6mm (ES)</option>
                                                            <option @if(strpos($result->variant, 'Wrap Type 3mm') !== false) selected @endif value="Wrap Type 3mm (WT">Wrap Type 3mm (WT)</option>
                                                        </select>
                                                    </div>

                                                @endif
                                                <br>
                                                <br>
                                                @if($result->brand != 6)
                                                <div class="form-group">

                                                    <h5>Other Properties</h5>
                                                    <hr>
                                                </div>
                                                @if($request->brand == 1)
                                                    <div class="form-group">

                                                        <!-- <label class="control-label mb-10">Default switches</label> -->
                                                            <div class="control-label mb-10">
                                                                <label>
                                                                    Parking Sensors
                                                                </label>
                                                                <input id="check_box_switch" name="parking" type="checkbox" data-off-text="False" data-on-text="True" class="bs-switch">

                                                            </div>


                                                    </div>
                                                    <br>
                                                @endif
                                                <div class="form-group">
                                                    <!-- <label class="control-label mb-10">Default switches</label> -->
                                                        <div class="control-label mb-10">
                                                            <label>
                                                                Magnetic Backing
                                                            </label>
                                                            <input id="check_box_switch" name="magnetic" type="checkbox" data-off-text="False" data-on-text="True" class="bs-switch" >

                                                        </div>


                                                </div>
                                                <br>
                                                <div class="form-group">

                                                    <!-- <label class="control-label mb-10">Default switches</label> -->
                                                    <div class="control-label mb-10">
                                                        <label>
                                                            Microskin Backing
                                                        </label>
                                                        <input id="check_box_switch" name="microskin" type="checkbox" data-off-text="False" data-on-text="True" class="bs-switch"  >

                                                    </div>

                                                </div>
                                                <br>
                                                <div class="form-group">

                                                    <!-- <label class="control-label mb-10">Default switches</label> -->

                                                    <div class="control-label mb-10">
                                                        <label>
                                                            Stone Guard Laminate
                                                        </label>
                                                        <input id="check_box_switch" name="stone_guard" type="checkbox" data-off-text="False" data-on-text="True" class="bs-switch"  >

                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">

                                                    <!-- <label class="control-label mb-10">Default switches</label> -->
                                                    @if($result->brand == 1 || $result->brand == 3)
                                                    <div class="control-label mb-10">
                                                        <label>
                                                            Painted Bumpers
                                                        </label>
                                                        <input id="check_box_switch" name="painted" type="checkbox" data-off-text="False" data-on-text="True" class="bs-switch" >

                                                    </div>
                                                    @endif
                                                </div>
                                                    @endif
                                            </div>

                                        </div>


                                        <br>
                                        <br>
                                        <hr>
                                        <div class="form-group text-center">

                                            <button type="submit" class="btn btn-info btn-rounded">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>

    </div>

    <!--  <a href="#!" data-toggle="modal" data-target="#myModal">Modal</a> -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="background: transparent;box-shadow: none;text-align: center;">
                <form action="{{url('importExcel')}}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <img src="{{url('loading.gif')}}">
                </div>
                </form>
            </div>
        </div>
    </div>


@stop

@section('foot')
    @parent
    <script>
        $(document).ready(function () {

            @if($result->magnetic == 'on')
                $('input[name=magnetic]').click();
            @endif
            @if($result->microskin == 'on')
                $('input[name=microskin]').click();
            @endif
            @if($result->stone_guard == 'on')
                $('input[name=stone_guard]').click();
            @endif
            @if($result->painted == 'on')
                $('input[name=painted]').click();
            @endif
            @if($result->parking == 'on')
                $('input[name=parking]').click();
            @endif


            $('#check1').on('change', function () {
                
                var id = $(this).val();
                $('#myModal').modal('show');
               $.ajax({
                   method: 'GET',
                   url: '{{ url("admin/model") }}/'+id,
                   success: function (model) {

                       $('#check2').html(" ");
                       $('#check3').html(" ");
                       $('#check4').html(" ");
                       $('#check5').html(" ");
                       $('#check6').html(" ");
                       $('#check7').html(" ");
                       $('#check8').html(" ");
                       for(i = 0; i < model.length; i++){
                             if(i == 0){
                                $('#check2').append("<option selected disabled value=''>Select an option</option>");

                             }
                                 $('#check2').append("<option value='" + model[i]["id"] + "'>" + model[i]["name"] + "</option>");

                        }
                        $('#myModal').modal('hide');
                   }
               });
            });

            $('#check2').on('change', function () {
                var id = $(this).val();
                $('#myModal').modal('show');
               $.ajax({
                   method: 'GET',
                   url: '{{ url("admin/year") }}/'+id,
                   success: function (year) {

                       $('#check3').html(" ");
                       $('#check4').html(" ");
                       $('#check5').html(" ");
                       $('#check6').html(" ");
                       $('#check7').html(" ");
                       $('#check8').html(" ");
                       for(i = 0; i < year.length; i++){
                            if(i == 0){
                                $('#check3').append("<option selected disabled value=''>Select an option</option>");
                            }
                            $('#check3').append("<option value='"+year[i]["id"]+"'>"+year[i]["name"]+"</option>");
                        }
                        $('#myModal').modal('hide');
                   }
               });
            });

            $('#check3').on('change', function () {
                var id = $(this).val();
                $('#myModal').modal('show');
               $.ajax({
                   method: 'GET',
                   url: '{{ url("admin/all") }}/'+id,
                   success: function (all) {

                       $('#check4').html(" ");
                       $('#check5').html(" ");
                       $('#check6').html(" ");
                       $('#check7').html(" ");
                       $('#check8').html(" ");
                       for(i = 0; i < all.wheel_base.length; i++){
                            if(i == 0){
                                $('#check4').append("<option selected  disabled value=''>Select an option</option>");
                            }
                            $('#check4').append("<option value='"+all.wheel_base[i]["id"]+"'>"+all.wheel_base[i]["name"]+' (' + all.wheel_base[i]["abbreviation"]+')'+"</option>");
                       }

                       for(i = 0; i < all.roof_config.length; i++){
                            if(i == 0){
                                $('#check5').append("<option selected disabled value=''>Select an option</option>");
                            }
                            $('#check5').append("<option value='"+all.roof_config[i]["id"]+"'>"+all.roof_config[i]["name"]+' (' + all.roof_config[i]["abbreviation"]+')'+"</option>");
                       }

                       for(i = 0; i < all.side_door_config.length; i++){
                            if(i == 0){
                                $('#check6').append("<option selected disabled value=''>Select an option</option>");
                            }
                            $('#check6').append("<option value='"+all.side_door_config[i]["id"]+"'>"+all.side_door_config[i]["name"]+' (' + all.side_door_config[i]["abbreviation"]+')'+"</option>");
                       }

                       for(i = 0; i < all.rear_door.length; i++){
                            if(i == 0){
                                $('#check7').append("<option selected disabled value=''>Select an option</option>");
                            }
                            $('#check7').append("<option value='"+all.rear_door[i]["id"]+"'>"+all.rear_door[i]["name"]+' (' + all.rear_door[i]["abbreviation"]+')'+"</option>");
                       }
                       for(i = 0; i < all.other_data.length; i++){
                            if(i == 0){
                                $('#check8').append("<option selected disabled value=''>Select an option</option>");
                            }
                            $('#check8').append("<option value='"+all.other_data[i]["id"]+"'>"+all.other_data[i]["parking"]+"</option>");
                       }

                       $('#myModal').modal('hide');
                   }
               });
            });
        });
    </script>
@endsection