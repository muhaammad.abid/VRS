@extends('admin.layout')
@section('title', 'List Result')
@section('content')
    <div class="page-wrapper" style="margin-left:0px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Welcome to <code>Vehicle Livery Solutions</code></h6>
                            </div>

                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">

                                 <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('vehicle_add')}}" class="btn btn-info btn-rounded btn-block btn-anim">
                                        <i class="fa fa-plus"></i>
                                        <span class="btn-text">Add New Vehicle</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="#" class="btn btn-primary btn-rounded btn-block btn-anim" data-toggle="modal" data-target="#myModal">
                                        <i class="fa fa-cloud-upload"></i>
                                        <span class="btn-text">Import</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('downloadExcel/'.'csv')}}" class="btn btn-success btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cloud-download"></i>
                                        <span class="btn-text">Export</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/results')}}" class="btn btn-default btn-rounded btn-block btn-anim">
                                        <i class="fa fa-list"></i>
                                        <span class="btn-text">Results</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('admin/product/settings')}}" class="btn btn-primary btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Product Settings</span>
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-12 mt-15">
                                    <a href="{{url('changepassword')}}" class="btn btn-warning btn-rounded btn-block btn-anim">
                                        <i class="fa fa-cog"></i>
                                        <span class="btn-text">Change Password</span>
                                    </a>
                                </div>


                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Import CSV</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{url('importExcel')}}" method="post" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="file-loading">
                                            <input id="input-b9" name="import_file" type="file">
                                        </div>
                                        <label class="radio-inline"><input type="radio" value="replace" name="clear">Replace All</label>
                                        <label class="radio-inline"><input type="radio" value="udpate" checked="" name="clear">Update</label>
                                        <div id="kartik-file-errors"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary" title="Your custom upload logic">Uploads</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Results</h6>
                            </div>
                            <div class="pull-right">
                                @if(count($results) > 0)
                                    <a href="{{url('admin/results/export')}}" class="btn btn-success">Export</a>
                                @endif
                                <a href="{{url('admin/results/delete/all')}}" class="btn btn-warning">Delete All</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table @if(count($results) > 0) id="datable_1" @endif class="table table-hover display  pb-30">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>ShortName</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>ShortName</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            @if(count($results) > 0)

                                                @foreach($results as $row)
                                                    <tr>
                                                        <td>#ticket_{{$row->id}}</td>
                                                        <td>{{$row->name}}</td>
                                                        <td>{{$row->shortname}}</td>
                                                        <td>{{$row->description}}</td>
                                                        <td>
                                                            <a href="{{url('admin/result/edit/'.$row->id)}}" style="padding: 5px;margin: 5px;" class="btn col-lg-12 btn-info">Edit</a>
                                                            <a href="{{url('admin/result/delte/'.$row->id)}}" style="padding: 5px;margin: 5px;" class="btn col-lg-12 btn-danger">Delete</a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                @else

                                                <tr>
                                                    <td colspan="16">Record Not Found</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>
    </div>
@stop

@section('foot')
    @parent
    <script src="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js')}}"></script>
    <script>
        $(document).on('ready', function() {
            $("#input-b9").fileinput({
                showPreview: false,
                showUpload: false,
                elErrorContainer: '#kartik-file-errors',
                allowedFileExtensions: ["csv", "xls"]
                //uploadUrl: '/site/file-upload-single'
            });
        });
    </script>
@endsection