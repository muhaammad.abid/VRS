<!DOCTYPE html>
<html lang="en">
	<head>
		<title> Admin - @yield('title')</title>
		@include('admin.master.head')
	</head>
	<body style="background: #fff;">
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<div class="wrapper pa-0 ma-0 auth-page">
			@include('admin.master.header')
			@yield('content')
</div>
		{{--@section('foot')--}}
		{{--@show--}}
	@section('foot')
		@include('admin.master.foot')
	@show
	</body>
</html>