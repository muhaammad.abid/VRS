@extends('admin.layout')
@section('title', 'Updated Result')
@section('content')
    
    <!-- Main Content -->
            <div class="page-wrapper pa-0 ma-0 auth-page">
                <div class="container-fluid">
                    <!-- Row -->
                    <div class="table-struct full-width full-height">
                        <div class="table-cell vertical-align-middle auth-form-wrap">
                            <div class="auth-form  ml-auto mr-auto no-float">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="mb-30">
                                            <!-- <h3 class="text-center txt-dark mb-10">Sign up to Doodle</h3>
                                            <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6> -->
                                        </div>  
                                        <div class="form-wrap">
                                            <div class="mb-30">
                                                <h3 class="text-center txt-dark mb-10">ID: <code>#ticket_{{$new->id}}</code></h3>
                                                <h6 class="text-center nonecase-font txt-grey">Generated details are as below</h6>
                                            </div>
                                                <div class="form-group">
                                                    <label class="control-label mb-10 text-left">Full Name </label>
                                                    <a href="#!"  class="btn btn-info copy" data-id="name" style="float: right;">Copy</a>
                                                    <textarea class="form-control" id="name" rows="5" readonly>{{$result['name']}}</textarea>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="control-label mb-10 text-left">Short Name</label>
                                                    <a href="#!"  class="btn btn-info copy" data-id="shortname" style="float: right;">Copy</a>
                                                    <textarea class="form-control" rows="3" id="shortname" readonly>{{$result['shortname']}}</textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label mb-10 text-left">Description</label>
                                                    <a href="#!"  class="btn btn-info copy" data-id="description" style="float: right;">Copy</a>
                                                    <textarea class="form-control" rows="10" id="description" readonly>{{$result['description']}}</textarea>
                                                </div>

                                                <div class="form-group text-center">
                                                    <a href="{{url('admin/results')}}" class="btn btn-info btn-rounded">Back To List</a>
                                                </div>
                                            
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Row -->   
                </div>
                
            </div>
            <!-- /Main Content -->

@stop

@section('foot')
    @parent
    <script type="text/javascript">
        $(document).ready(function(){

            $('.copy').on('click', function(){
                var id = $(this).data('id');
                $("#"+id).select();
                document.execCommand('copy');
            });

        });
    </script>
@endsection