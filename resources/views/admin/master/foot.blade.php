<!-- jQuery -->
<script src="{{url('files/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{url('files/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{url('files/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')}}"></script>

<!-- Data table JavaScript -->
<script src="{{url('files/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('files/dist/js/dataTables-data.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{url('files/dist/js/jquery.slimscroll.js')}}"></script>


<!-- Bootstrap Switch JavaScript -->
<script src="{{url('files/vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>

<script src="{{url('files/vendors/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

<script src="{{url('files/vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{url('files/vendors/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>

<script src="{{url('files/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
<!-- Form Advance Init JavaScript -->
<script src="{{url('files/dist/js/form-advance-data.js')}}"></script>


<!-- Init JavaScript -->
<script src="{{url('files/dist/js/init.js')}}"></script>