<header class="sp-header">
    <div class="sp-logo-wrap pull-left">
        <a href="{{url('admin/dashboard')}}">
            <img class="brand-img mr-10" src="{{url('files/dist/img/logo.png')}}" alt="brand" />
            <span class="brand-text">Vehicle Livery Solutions</span>
        </a>
    </div>
    <div class="form-group mb-0 pull-right">
        <!-- <span class="inline-block pr-10">Don't have an account?</span> -->
        <a class="inline-block btn btn-info btn-rounded btn-outline" href="{{url('logout')}}">logout</a>
    </div>
    <div class="clearfix"></div>
</header>