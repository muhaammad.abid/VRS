<!doctype html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    @include('front.master.head')
</head>
<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<div class="wrapper pa-0">
    <header class="sp-header">
        <div class="sp-logo-wrap pull-left">
            <a href="{{url('/')}}">
                <img class="brand-img mr-10" src="{{url('files/dist/img/logo.png')}}" alt="brand"/>
                <span class="brand-text">Vehicle Livery Solutions</span>
            </a>
        </div>
        <div class="form-group mb-0 pull-right">
            <!-- <span class="inline-block pr-10">Already have an account?</span> -->
            @if(Auth::check())
                <a class="inline-block btn btn-info btn-rounded btn-outline" href="{{url('admin/dashboard')}}">Admin Panel</a>
                @else
                <a class="inline-block btn btn-info btn-rounded btn-outline" href="{{url('login')}}">Admin Panel</a>
            @endif
        </div>
        <div class="clearfix"></div>
    </header>
    @yield('content')
</div>
@section('foot')
    @include('front.master.foot')
@show

</body>
</html>
