@extends('front.layout')

@section('title','Additional Information')
@section('content')

    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <!-- <h3 class="text-center txt-dark mb-10">Sign up to Doodle</h3>
                                        <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6> -->
                                </div>
                                <div class="form-wrap">
                                    <form action="{{ url('form_data') }}" method="GET">
                                        <input type="hidden" name="brand" value="{{ $request->brand}}">
                                        <div class="row">
                                            <div class="col-lg-12" style="margin-bottom: 25px;">

                                                <h3 style="text-align: center;">
                                                    @if($request->brand == 1)
                                                        Battenberg Kits
                                                    @elseif($request->brand == 2)
                                                        Chevron Kit
                                                    @elseif($request->brand == 3)
                                                        Escort Vehicle
                                                    @elseif($request->brand == 4)
                                                        Side Chevrons
                                                    @elseif($request->brand == 5)
                                                        Bonnet Markings
                                                    @elseif($request->brand == 6)
                                                        Templates
                                                    @endif
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <h5>Vehicle Information</h5>
                                                    <hr>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label mb-10">Make</label>
                                                    <select required="" id="check1" class="form-control select2" name="make">
                                                        <option selected disabled="" value="" >Select an option</option>
                                                        @foreach($makes as $make)
                                                            <option value="{{$make->id}}">{{$make->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label mb-10">Model</label>
                                                    <select required="" id="check2" class="form-control select2" name="model">
                                                        <option selected disabled="" value="" >Select an option</option>

                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label mb-10">Generation / [Year]</label>
                                                    <select required="" id="check3" class="form-control select2" name="year">
                                                        <option selected disabled="" value="" >Select an option</option>

                                                    </select>
                                                </div>

                                               @if($request->brand == 1)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Wheel Base</label>
                                                        <select required="" id="check4" class="form-control select2" name="wheel_base">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Roof Configuration</label>
                                                        <select required="" id="check5" class="form-control select2" name="roof_config">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Side Door Configuration</label>
                                                        <select required="" id="check6" class="form-control select2" name="side_door_config">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Rear Door Configuration</label>
                                                        <select required="" id="check7" class="form-control select2" name="rear_door">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label class="control-label mb-10">Parking Sensors</label>
                                                        <select required="" id="check8" class="form-control select2" name="parking">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div> -->
                                               @elseif($request->brand == 2)

                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Roof Configuration</label>
                                                        <select required="" id="check5" class="form-control select2" name="roof_config">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Rear Door Configuration</label>
                                                        <select required="" id="check7" class="form-control select2" name="rear_door">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>

                                                @elseif($request->brand == 3)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Wheel Base</label>
                                                        <select required="" id="check4" class="form-control select2" name="wheel_base">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Roof Configuration</label>
                                                        <select required="" id="check5" class="form-control select2" name="roof_config">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Side Door Configuration</label>
                                                        <select required="" id="check6" class="form-control select2" name="side_door_config">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Rear Door Configuration</label>
                                                        <select required="" id="check7" class="form-control select2" name="rear_door">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>

                                                @elseif($request->brand == 4)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Wheel Base</label>
                                                        <select required="" id="check4" class="form-control select2" name="wheel_base">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                @elseif($request->brand == 5)

                                                @elseif($request->brand == 6)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Wheel Base</label>
                                                        <select required="" id="check4" class="form-control select2" name="wheel_base">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Roof Configuration</label>
                                                        <select required="" id="check5" class="form-control select2" name="roof_config">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Side Door Configuration</label>
                                                        <select required="" id="check6" class="form-control select2" name="side_door_config">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Rear Door Configuration</label>
                                                        <select required="" id="check7" class="form-control select2" name="rear_door">
                                                            <option selected disabled value="">Select an option</option>
                                                        </select>
                                                    </div>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <h5>Product Information</h5>
                                                    <hr>
                                                </div>
                                                @if($request->brand == 1)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Battenburg Variant</label>
                                                        <select required="" id="check9" class="form-control select2" name="variant">
                                                            <option selected disabled value="">Select an option</option>
                                                            @if(count($battens) > 0)
                                                                @foreach($battens as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Material</label>
                                                        <select required="" id="check10" class="form-control select2" name="material">
                                                            <option selected disabled value="">Select an option</option>
                                                            @if(count($materials) > 0)
                                                                @foreach($materials as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label mb-10">End Use</label>
                                                        <select required="" id="check11" class="form-control select2" name="end_user">
                                                            <option selected disabled value="">Select an option</option>
                                                            @if(count($enduses) > 0)
                                                                @foreach($enduses as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                @elseif($request->brand == 2)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Chevron Kit Variant</label>
                                                        <select required="" id="check9" class="form-control select2" name="variant">
                                                            <option selected disabled value="">Select an option</option>
                                                            @if(count($chevrons) > 0)
                                                                @foreach($chevrons as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Grade</label>
                                                        <select required="" id="check10" class="form-control select2" name="grade">
                                                            <option selected disabled value="">Select an option</option>
                                                            @if(count($grades) > 0)
                                                                @foreach($grades as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>

                                                @elseif($request->brand == 3)
                                                     <div class="form-group">
                                                        <label class="control-label mb-10">Escort Vehicle Variant</label>
                                                        <select required="" id="check9" class="form-control select2" name="variant">
                                                            <option selected disabled value="">Select an option</option>
                                                            @if(count($escorts) > 0)
                                                                @foreach($escorts as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label class="control-label mb-10">Material</label>
                                                        <select required="" id="check10" class="form-control select2" name="material">
                                                            <option selected disabled value="">Select an option</option>
                                                            @if(count($materials) > 0)
                                                                @foreach($materials as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div> -->
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Grade</label>
                                                        <select required="" id="check10" class="form-control select2" name="grade">
                                                            <option selected disabled value="">Select an option</option>
                                                            @if(count($grades) > 0)
                                                                @foreach($grades as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                   <!--  <div class="form-group">
                                                        <label class="control-label mb-10">End Use</label>
                                                        <select required="" id="check11" class="form-control select2">
                                                            <option selected disabled value="">Select an option</option>
                                                            <option value="2">Police Forces</option>
                                                            <option value="3">Ambulance and Doctors</option>
                                                            <option value="4">Fire and Rescue</option>
                                                        </select>
                                                    </div> -->
                                                @elseif($request->brand == 4)

                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Grade</label>
                                                        <select required="" id="check10" class="form-control select2" name="grade">
                                                            <option selected disabled value="">Select an option</option>
                                                             @if(count($grades) > 0)
                                                                @foreach($grades as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                @elseif($request->brand == 5)
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Bonnet Marking Variants</label>
                                                        <select required="" id="check9" class="form-control select2" name="variant">
                                                            <option selected disabled value="">Select an option</option>
                                                             @if(count($bonnets) > 0)
                                                                @foreach($bonnets as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Grade</label>
                                                        <select required="" id="check10" class="form-control select2" name="grade">
                                                            <option selected disabled value="">Select an option</option>
                                                             @if(count($grades) > 0)
                                                                @foreach($grades as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>

                                                @else
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Templates Variant</label>
                                                        <select required="" id="check9" class="form-control select2" name="variant">
                                                            <option selected disabled value="">Select an option</option>
                                                            @if(count($templates) > 0)
                                                                @foreach($templates as $bat)
                                                                    <option value="{{$bat->name}} ( {{$bat->abbreviation}}">{{$bat->name}} ({{$bat->abbreviation}})</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>

                                                @endif
                                                <br>
                                                <br>
                                                @if($request->brand != 6)
                                                <div class="form-group">

                                                    <h5>Other Properties</h5>
                                                    <hr>
                                                </div>
                                                @if($request->brand == 1)
                                                    <div class="form-group">

                                                        <!-- <label class="control-label mb-10">Default switches</label> -->
                                                            <div class="control-label mb-10">
                                                                <label>
                                                                    Parking Sensors
                                                                </label>
                                                                <input id="check_box_switch" name="parking" type="checkbox" data-off-text="False" data-on-text="True" class="bs-switch">

                                                            </div>


                                                    </div>
                                                    <br>
                                                @endif
                                                <div class="form-group">

                                                    <!-- <label class="control-label mb-10">Default switches</label> -->
                                                        <div class="control-label mb-10">
                                                            <label>
                                                                Magnetic Backing
                                                            </label>
                                                            <input id="check_box_switch" name="magnetic" type="checkbox" data-off-text="False" data-on-text="True" class="bs-switch">

                                                        </div>


                                                </div>
                                                <br>
                                                <div class="form-group">

                                                    <!-- <label class="control-label mb-10">Default switches</label> -->
                                                    <div class="control-label mb-10">
                                                        <label>
                                                            Microskin Backing
                                                        </label>
                                                        <input id="check_box_switch" name="microskin" type="checkbox" data-off-text="False" data-on-text="True" class="bs-switch">

                                                    </div>

                                                </div>
                                                <br>
                                                <div class="form-group">

                                                    <!-- <label class="control-label mb-10">Default switches</label> -->

                                                    <div class="control-label mb-10">
                                                        <label>
                                                            Stone Guard Laminate
                                                        </label>
                                                        <input id="check_box_switch" name="stone_guard" type="checkbox" data-off-text="False" data-on-text="True" class="bs-switch">

                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">

                                                    <!-- <label class="control-label mb-10">Default switches</label> -->
                                                    @if($request->brand == 1 || $request->brand == 3)
                                                    <div class="control-label mb-10">
                                                        <label>
                                                            Painted Bumpers
                                                        </label>
                                                        <input id="check_box_switch" name="painted" type="checkbox" data-off-text="False" data-on-text="True" class="bs-switch">

                                                    </div>
                                                    @endif
                                                </div>
                                                @endif
                                            </div>

                                        </div>


                                        <br>
                                        <br>
                                        <hr>
                                        <div class="form-group text-center">

                                            <button type="submit" class="btn btn-info btn-rounded">Generate</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>

    </div>
   <!--  <a href="#!" data-toggle="modal" data-target="#myModal">Modal</a> -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="background: transparent;box-shadow: none;text-align: center;">
                <form action="{{url('importExcel')}}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <img src="{{url('loading.gif')}}">
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('foot')
    @parent
    <script>
        $(document).ready(function () {
            $('#check1').on('change', function () {
                
                $('#myModal').modal('show');

                var id = $(this).val();
               $.ajax({
                   method: 'GET',
                   url: '{{ url('model') }}/'+id,
                   success: function (model) {

                       $('#check2').html(" ");
                       for(i = 0; i < model.length; i++){
                             if(i == 0){
                                $('#check2').append("<option selected disabled  value=''>Select an option</option>");

                             }
                                 $('#check2').append("<option value='" + model[i]["id"] + "'>" + model[i]["name"] + "</option>");

                        }

                        $('#myModal').modal('hide');
                        $('#check2').closest('.form-group').find('.select2-search__field').focus();
                   }
               });
            });

            $('#check2').on('change', function () {
                $('#myModal').modal('show');
                var id = $(this).val();
               $.ajax({
                   method: 'GET',
                   url: '{{ url('year') }}/'+id,
                   success: function (year) {

                       $('#check3').html(" ");
                       for(i = 0; i < year.length; i++){
                            if(i == 0){
                                $('#check3').append("<option selected disabled value=''>Select an option</option>");

                            }
                            $('#check3').append("<option value='"+year[i]["id"]+"'>"+year[i]["name"]+"</option>");
                        }

                        $('#myModal').modal('hide');
                   }
               });
            });

            $('#check3').on('change', function () {
                $('#myModal').modal('show');
                var id = $(this).val();
               $.ajax({
                   method: 'GET',
                   url: '{{ url('all') }}/'+id,
                   success: function (all) {

                       $('#check4').html(" ");
                       $('#check5').html(" ");
                       $('#check6').html(" ");
                       $('#check7').html(" ");
                       $('#check8').html(" ");
                       for(i = 0; i < all.wheel_base.length; i++){
                            if(i == 0){
                                $('#check4').append("<option selected disabled value=''>Select an option</option>");
                            }
                            $('#check4').append("<option value='"+all.wheel_base[i]["id"]+"'>"+all.wheel_base[i]["name"]+' (' + all.wheel_base[i]["abbreviation"]+')'+"</option>");
                       }

                       for(i = 0; i < all.roof_config.length; i++){
                            if(i == 0){
                                $('#check5').append("<option selected disabled value=''>Select an option</option>");
                            }
                            $('#check5').append("<option value='"+all.roof_config[i]["id"]+"'>"+all.roof_config[i]["name"]+' (' + all.roof_config[i]["abbreviation"]+')'+"</option>");
                       }

                       for(i = 0; i < all.side_door_config.length; i++){
                            if(i == 0){
                                $('#check6').append("<option selected disabled value=''>Select an option</option>");
                            }
                            $('#check6').append("<option value='"+all.side_door_config[i]["id"]+"'>"+all.side_door_config[i]["name"]+' (' + all.side_door_config[i]["abbreviation"]+')'+"</option>");
                       }

                       for(i = 0; i < all.rear_door.length; i++){
                            if(i == 0){
                                $('#check7').append("<option selected disabled value=''>Select an option</option>");
                            }
                            $('#check7').append("<option value='"+all.rear_door[i]["id"]+"'>"+all.rear_door[i]["name"]+' (' + all.rear_door[i]["abbreviation"]+')'+"</option>");
                       }
                       for(i = 0; i < all.other_data.length; i++){
                            if(i == 0){
                                $('#check8').append("<option selected disabled value=''>Select an option</option>");
                            }
                            $('#check8').append("<option value='"+all.other_data[i]["id"]+"'>"+all.other_data[i]["parking"]+"</option>");
                       }

                       $('#myModal').modal('hide');
                   }
               });
            });
        });
    </script>
@endsection
