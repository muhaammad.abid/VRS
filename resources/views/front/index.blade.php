@extends('front.layout')

@section('title','Vehicle Livery Solutions')
@section('content')
	<div class="wrapper pa-0">
		<!-- Main Content -->
		<div class="page-wrapper pa-0 ma-0 auth-page">
			<div class="container-fluid">
				<!-- Row -->
				<div class="table-struct full-width full-height">
					<div class="table-cell vertical-align-middle auth-form-wrap">
						<div class="auth-form  ml-auto mr-auto no-float">
							<div class="row">
								<div class="col-sm-12 col-xs-12">
									<div class="mb-30">
										<h3 class="text-center txt-dark mb-10">Select Product Type</h3>
										<!-- <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6> -->
									</div>
									<div class="form-wrap">
										<form action="{{url('step1')}}" id="form" method="get" >
											<div class="form-group">
												@if(session('error'))
													<p class="alert alert-danger">{{session('error')}}</p>
												@endif
												<!-- <label class="control-label mb-10">Product Type</label> -->
												<select class="form-control select2" name="brand" required="">
													<option value="">Select an option</option>
													<option value="1" name="Battenberg Kits">Battenberg Kits</option>
													<option value="2" name="Chevron Kit">Chevron Kit</option>
													<option value="3" name="Escort Vehicle">Escort Vehicle</option>
													<option value="4" name="Side Chevrons">Side Chevrons</option>
													<option value="5" name="Bonnet Markings">Bonnet Markings</option>
													<option value="6" name="Templates">Templates</option>
												</select>
											</div>


											<div class="form-group text-center">
												<button type="submit" class="btn btn-info btn-rounded">Next</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>

		</div>
		<!-- /Main Content -->

	</div>
@endsection
@section('foot')
	@parent
	<script>
//		$(document).ready(function () {
//            $('.select2').on('change', function(){
//
//                var value = $(this).val();
//                $('input[name=type]').val(value);
////                $('#form').submit();
//
//            });
//
//        });
	</script>
@endsection
