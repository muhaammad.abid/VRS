<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="devstudio" />

<!-- Favicon -->
<link rel="shortcut icon" href="favicon.ico">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- vector map CSS -->
<link href="{{url('files/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>

<!-- select2 CSS -->
<link href="{{url('files/vendors/bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('files/vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- Custom CSS -->
<link href="{{url('files/dist/css/style.css')}}" rel="stylesheet" type="text/css">