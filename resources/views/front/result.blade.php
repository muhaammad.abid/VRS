<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>VLS</title>
		<meta name="description" content="Natural Language Form with custom text input and drop-down lists" />
		<meta name="keywords" content="Natural Language UI, sentence form, text input, contenteditable, html5, css3, jquery" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="{{url('assets/front/css/default.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{url('assets/front/css/component.css')}}" />
		<script src="js/modernizr.custom.js"></script>
	</head>
	<body class="nl-blurred">
		<div class="container demo-1">
			<!-- Top Navigation -->
			<div class="codrops-top clearfix">
				<a class="codrops-icon codrops-icon-prev" href="{{url('/')}}"><span>Go Back</span></a>
				<span class="right"><a class="codrops-icon codrops-icon-drop" href="{{url('login')}}"><span>Admin Login</span></a></span>
			</div>
			<header>
				<h1>Vehicle Livery Solutions</h1>
			</header>
			<div class="main clearfix">
				<h2>Name</h2>
				@if($data['brand'] == 1)
					<p>@if(isset($data['variant'][1]))
							{{ $data['variant'][1] }}
						@endif
						{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['wheel_base']->abbreviation)) 
							{{ $data['wheel_base']->abbreviation }}
						@endif
						@if(isset($data['roof_config']->abbreviation))
							{{ $data['roof_config']->abbreviation }}   
						@endif
						@if(isset($data['rear_door']->abbreviation))
							{{ $data['rear_door']->abbreviation }} 
						@endif
						@if(isset($data['side_door_config']->abbreviation))
							{{ $data['side_door_config']->abbreviation }}
						@endif
						@if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][1]))
							{{ $data['end_user'][1] }} 
						@endif
						@if(isset($data['material'][1]))
							{{ $data['material'][1] }} 
						@endif
						@if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}
						@endif
					</p>
					<br>
					<hr>
					
					<h2>Description</h2>
					<p>@if(isset($data['variant'][0]))
							{{ $data['variant'][0] }}
						@endif
						{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['wheel_base']->name)) 
							{{ $data['wheel_base']->name }}
						@endif
						@if(isset($data['roof_config']->name))
							{{ $data['roof_config']->name }}   
						@endif
						@if(isset($data['rear_door']->name))
							{{ $data['rear_door']->name }} 
						@endif
						@if(isset($data['side_door_config']->name))
							{{ $data['side_door_config']->name }}
						@endif
						@if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][0]))
							{{ $data['end_user'][0] }} 
						@endif
						@if(isset($data['material'][0]))
							{{ $data['material'][0] }} 
						@endif
						@if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}.
						@endif
					</p>
					<br>

				@elseif($data['brand'] == 2)
						<p>{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['wheel_base']->abbreviation)) 
							{{ $data['wheel_base']->abbreviation }}
						@endif
						@if(isset($data['roof_config']->abbreviation))
							{{ $data['roof_config']->abbreviation }}   
						@endif
						@if(isset($data['rear_door']->abbreviation))
							{{ $data['rear_door']->abbreviation }} 
						@endif
						@if(isset($data['variant'][1]))
							{{ $data['variant'][1] }}
						@endif
						@if(isset($data['grade']))
							{{ $data['grade'] }}
						@endif
						{{-- @if(isset($data['side_door_config']->abbreviation))
							{{ $data['side_door_config']->abbreviation }}
						@endif
						@if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][1]))
							{{ $data['end_user'][1] }} 
						@endif
						@if(isset($data['material'][1]))
							{{ $data['material'][1] }} 
						@endif
						@if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}
						@endif --}}
					</p>
					<br>
					<hr>
					{{-- <h2>Short Name</h2>
					<p>Sprinter Mk2 2006 - 2016 MWB SD HR DD Full Battenburg 3M</p>
					<br>
					<hr> --}}
					<h2>Description</h2>
					<p>{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['wheel_base']->name)) 
							{{ $data['wheel_base']->name }}
						@endif
						@if(isset($data['roof_config']->name))
							{{ $data['roof_config']->name }}   
						@endif
						@if(isset($data['rear_door']->name))
							{{ $data['rear_door']->name }} 
						@endif
						@if(isset($data['variant'][0]))
							{{ $data['variant'][0] }}
						@endif
						@if(isset($data['grade']))
							{{ $data['grade'] }}
						@endif
						{{-- @if(isset($data['side_door_config']->abbreviation))
							{{ $data['side_door_config']->abbreviation }}
						@endif
						@if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][1]))
							{{ $data['end_user'][1] }} 
						@endif
						@if(isset($data['material'][1]))
							{{ $data['material'][1] }} 
						@endif
						@if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}
						@endif --}}
					</p>
					<br>
				@elseif($data['brand'] == 3)
					<p>{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['wheel_base']->abbreviation)) 
							{{ $data['wheel_base']->abbreviation }}
						@endif
						@if(isset($data['roof_config']->abbreviation))
							{{ $data['roof_config']->abbreviation }}   
						@endif
						@if(isset($data['side_door_config']->abbreviation))
							{{ $data['side_door_config']->abbreviation }}
						@endif
						@if(isset($data['rear_door']->abbreviation))
							{{ $data['rear_door']->abbreviation }} 
						@endif
						{{-- @if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][1]))
							{{ $data['end_user'][1] }} 
						@endif --}}
						@if(isset($data['material'][1]))
							{{ $data['material'][1] }} 
						@endif
						{{-- @if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}
						@endif --}}
					</p>
					<br>
					<hr>
					{{-- <h2>Short Name</h2>
					<p>Sprinter Mk2 2006 - 2016 MWB SD HR DD Full Battenburg 3M</p>
					<br>
					<hr> --}}
					<h2>Description</h2>
					<p>{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['wheel_base']->name)) 
							{{ $data['wheel_base']->name }}
						@endif
						@if(isset($data['roof_config']->name))
							{{ $data['roof_config']->name }}   
						@endif
						@if(isset($data['side_door_config']->name))
							{{ $data['side_door_config']->name }}
						@endif
						@if(isset($data['rear_door']->name))
							{{ $data['rear_door']->name }} 
						@endif
						{{-- @if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][0]))
							{{ $data['end_user'][0] }} 
						@endif --}}
						@if(isset($data['material'][0]))
							{{ $data['material'][0] }} 
						@endif
						{{-- @if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}.
						@endif --}}
					</p>
					<br>
				@elseif($data['brand'] == 4)
					<p>{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['wheel_base']->abbreviation)) 
							{{ $data['wheel_base']->abbreviation }}
						@endif
						{{-- @if(isset($data['roof_config']->abbreviation))
							{{ $data['roof_config']->abbreviation }}   
						@endif
						@if(isset($data['rear_door']->abbreviation))
							{{ $data['rear_door']->abbreviation }} 
						@endif
						@if(isset($data['side_door_config']->abbreviation))
							{{ $data['side_door_config']->abbreviation }}
						@endif
						@if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][1]))
							{{ $data['end_user'][1] }} 
						@endif
						@if(isset($data['material'][1]))
							{{ $data['material'][1] }} 
						@endif
						@if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}
						@endif --}}
						@if(isset($data['grade']))
							{{ $data['grade'] }}
						@endif
					</p>
					<br>
					<hr>
					{{-- <h2>Short Name</h2>
					<p>Sprinter Mk2 2006 - 2016 MWB SD HR DD Full Battenburg 3M</p>
					<br>
					<hr> --}}
					<h2>Description</h2>
					<p>{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['wheel_base']->name)) 
							{{ $data['wheel_base']->name }}
						@endif
						{{-- @if(isset($data['roof_config']->name))
							{{ $data['roof_config']->name }}   
						@endif
						@if(isset($data['rear_door']->name))
							{{ $data['rear_door']->name }} 
						@endif
						@if(isset($data['side_door_config']->name))
							{{ $data['side_door_config']->name }}
						@endif
						@if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][0]))
							{{ $data['end_user'][0] }} 
						@endif
						@if(isset($data['material'][0]))
							{{ $data['material'][0] }} 
						@endif
						@if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}.
						@endif --}}
						@if(isset($data['grade']))
							{{ $data['grade'] }}
						@endif
					</p>
					<br>
				@elseif($data['brand'] == 5)
					 <p>{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['variant'][1]))
							{{ $data['variant'][1] }}
						@endif
						{{-- @if(isset($data['wheel_base']->abbreviation)) 
							{{ $data['wheel_base']->abbreviation }}
						@endif
						@if(isset($data['roof_config']->abbreviation))
							{{ $data['roof_config']->abbreviation }}   
						@endif
						@if(isset($data['rear_door']->abbreviation))
							{{ $data['rear_door']->abbreviation }} 
						@endif
						@if(isset($data['side_door_config']->abbreviation))
							{{ $data['side_door_config']->abbreviation }}
						@endif
						@if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][1]))
							{{ $data['end_user'][1] }} 
						@endif
						@if(isset($data['material'][1]))
							{{ $data['material'][1] }} 
						@endif
						@if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}
						@endif --}}
						@if(isset($data['grade']))
							{{ $data['grade'] }}
						@endif
					</p>
					<br>
					<hr>
					{{-- <h2>Short Name</h2>
					<p>Sprinter Mk2 2006 - 2016 MWB SD HR DD Full Battenburg 3M</p>
					<br>
					<hr> --}}
					<h2>Description</h2>
					<p>{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['variant'][0]))
							{{ $data['variant'][0] }}
						@endif
						{{-- @if(isset($data['wheel_base']->name)) 
							{{ $data['wheel_base']->name }}
						@endif
						@if(isset($data['roof_config']->name))
							{{ $data['roof_config']->name }}   
						@endif
						@if(isset($data['rear_door']->name))
							{{ $data['rear_door']->name }} 
						@endif
						@if(isset($data['side_door_config']->name))
							{{ $data['side_door_config']->name }}
						@endif
						@if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][0]))
							{{ $data['end_user'][0] }} 
						@endif
						@if(isset($data['material'][0]))
							{{ $data['material'][0] }} 
						@endif
						@if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}.
						@endif --}}
						@if(isset($data['grade']))
							{{ $data['grade'] }}
						@endif
					</p>
					<br>
				@else
					<p>
						{{-- @if(isset($data['variant'][1]))
							{{ $data['variant'][1] }}
						@endif --}}
						{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['wheel_base']->abbreviation)) 
							{{ $data['wheel_base']->abbreviation }}
						@endif
						@if(isset($data['roof_config']->abbreviation))
							{{ $data['roof_config']->abbreviation }}   
						@endif
						@if(isset($data['side_door_config']->abbreviation))
							{{ $data['side_door_config']->abbreviation }}
						@endif
						@if(isset($data['rear_door']->abbreviation))
							{{ $data['rear_door']->abbreviation }} 
						@endif
						{{-- @if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][1]))
							{{ $data['end_user'][1] }} 
						@endif
						@if(isset($data['material'][1]))
							{{ $data['material'][1] }} 
						@endif
						@if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}
						@endif --}}
						@if(isset($data['grade']))
							{{ $data['grade'] }}
						@endif

					</p>
					<br>
					<hr>
					{{-- <h2>Short Name</h2>
					<p>Sprinter Mk2 2006 - 2016 MWB SD HR DD Full Battenburg 3M</p>
					<br>
					<hr> --}}
					<h2>Description</h2>
					<p>
						{{-- @if(isset($data['variant'][0]))
							{{ $data['variant'][0] }}
						@endif --}}
						{{ $data['make']->name }} {{ $data['model']->name }} {{ $data['year']->name }} 
						@if(isset($data['wheel_base']->name)) 
							{{ $data['wheel_base']->name }}
						@endif
						@if(isset($data['roof_config']->name))
							{{ $data['roof_config']->name }}   
						@endif
						@if(isset($data['side_door_config']->name))
							{{ $data['side_door_config']->name }}
						@endif
						@if(isset($data['rear_door']->name))
							{{ $data['rear_door']->name }} 
						@endif
						{{-- @if(isset($data['parking'])) 
							{{ $data['parking'] }} 
						@endif 
						@if(isset($data['painted']))
							{{ $data['painted'] }}
						@endif
						@if(isset($data['end_user'][0]))
							{{ $data['end_user'][0] }} 
						@endif
						@if(isset($data['material'][0]))
							{{ $data['material'][0] }} 
						@endif
						@if(isset($data['magnetic']))
							{{ $data['magnetic'] }}
						@endif
						@if(isset($data['microskin']))
							{{ $data['microskin'] }}
						@endif
						@if(isset($data['stone_guard']))
							{{ $data['stone_guard'] }}.
						@endif --}}
					</p>
					<br>
				@endif
				
			</div>
		</div><!-- /container -->
		<script src="{{url('assets/front/js/nlform.js')}}"></script>
		<script>
		var nlform = new NLForm(document.getElementById("nl-form"));
</script>
	</body>
</html>
