<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Result</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="devstudio" />
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="{{url('files/favicon.ico')}}">
		<link rel="icon" href="{{url('files/favicon.ico')}}" type="image/x-icon">
		
		<!-- vector map CSS -->
		<link href="{{url('files/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
		
		<!-- select2 CSS -->
		<link href="{{url('files/vendors/bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
		
		<!-- Custom CSS -->
		<link href="{{url('files/dist/css/style.css')}}" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="{{url('/')}}">
						<img class="brand-img mr-10" src="{{url('files/dist/img/logo.png')}}" alt="brand"/>
						<span class="brand-text">Vehicle Livery Solutions</span>
					</a>
				</div>
				<div class="form-group mb-0 pull-right">
					<!-- <span class="inline-block pr-10">Already have an account?</span> -->
					@if(Auth::check())
		                <a class="inline-block btn btn-info btn-rounded btn-outline" href="{{url('admin/dashboard')}}">Admin Panel</a>
		                @else
		                <a class="inline-block btn btn-info btn-rounded btn-outline" href="{{url('login')}}">Admin Panel</a>
		            @endif
				</div>
				<div class="clearfix"></div>
			</header>
			
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<!-- <h3 class="text-center txt-dark mb-10">Sign up to Doodle</h3>
											<h6 class="text-center nonecase-font txt-grey">Enter your details below</h6> -->
										</div>	
										<div class="form-wrap">
											<div class="mb-30">
												<h3 class="text-center txt-dark mb-10">ID: <code>#ticket_{{$new->id}}</code></h3>
												<h6 class="text-center nonecase-font txt-grey">Generated details are as below</h6>
											</div>
												<div class="form-group">
													<label class="control-label mb-10 text-left">Full Name </label>
													<a href="#!"  class="btn btn-info copy" data-id="name" style="float: right;">Copy</a>
													<textarea class="form-control" rows="5" id="name" readonly>{{$result['name']}}</textarea>
												</div>
												
												<div class="form-group">
													<label class="control-label mb-10 text-left">Short Name</label>
													<a href="#!"  class="btn btn-info copy" data-id="shortname" style="float: right;">Copy</a>
													<textarea class="form-control" rows="3" id="shortname" readonly>{{$result['shortname']}}</textarea>
												</div>

												<div class="form-group">
													<label class="control-label mb-10 text-left">Description</label>
													<a href="#!"  class="btn btn-info copy" data-id="description" style="float: right;">Copy</a>
													<textarea class="form-control" rows="10" id="description" readonly>{{$result['description']}}</textarea>
												</div>

												<div class="form-group text-center">
													<a href="{{url('/')}}" class="btn btn-info btn-rounded">Create New Product</a>
												</div>
											
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
		<script src="{{url('files/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="{{url('files/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
		<script src="{{url('files/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')}}"></script>

		<!-- Select2 JavaScript -->
		<script src="{{url('files/vendors/bower_components/select2/dist/js/select2.full.min.js')}}"></script>


		<!-- Slimscroll JavaScript -->
		<script src="{{url('files/dist/js/jquery.slimscroll.js')}}"></script>

		<!-- Bootstrap Switch JavaScript -->
		<script src="{{url('files/vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>
		<!-- Form Advance Init JavaScript -->
		<script src="{{url('files/dist/js/form-advance-data.js')}}"></script>

		<!-- Init JavaScript -->
		<script src="{{url('files/dist/js/init.js')}}"></script>
		<script type="text/javascript">
        $(document).ready(function(){

            $('.copy').on('click', function(){
                var id = $(this).data('id');
                $("#"+id).select();
                document.execCommand('copy');
            });

        });
    </script>
	</body>
</html>
