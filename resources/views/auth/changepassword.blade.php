<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>VLS - Admin</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="devstudio" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- vector map CSS -->
    <link href="{{url('files/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>



    <!-- Custom CSS -->
    <link href="{{url('files/dist/css/style.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
<!--Preloader--
><div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->

<div class="wrapper pa-0">
    <header class="sp-header">
        <div class="sp-logo-wrap pull-left">
            <a href="{{url('admin/dashboard')}}">
                <img class="brand-img mr-10" src="{{url('files/dist/img/logo.png')}}" alt="brand" />
                <span class="brand-text">Vehicle Livery Solutions</span>
            </a>
        </div>
        <div class="form-group mb-0 pull-right">
            <!-- <span class="inline-block pr-10">Don't have an account?</span> -->
            {{--<a class="inline-block btn btn-info btn-rounded btn-outline" href="index.html">Create New Battenburg</a>--}}
        </div>
        <div class="clearfix"></div>
    </header>

    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                @if(session('success'))
                                    <div class="alert alert-success">
                                        <strong>Success! </strong>{{session('success')}}
                                    </div>
                                @endif
                                <div class="mb-30">
                                    <h3 class="text-center txt-dark mb-10"><br>Change Password</h3>
                                    <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                                </div>
                                <div class="form-wrap">
                                    <form action="{{url('savepassword')}}" method="post" id="loginForm" name="loginForm">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <label class="pull-left control-label mb-10" for="exampleInputpwd_2">Enter New Password</label>
                                            <input type="password" class="form-control" required="" name="password" id="password" placeholder="Enter New Password">
                                        </div>
                                        <div class="form-group">
                                            <label class="pull-left control-label mb-10" for="confirmpassword">Confirm Password</label>
                                            <!-- <a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="forgot-password.html">forgot password ?</a> -->
                                            <!-- <div class="clearfix"></div> -->
                                            <input type="password" class="form-control" required="" name="confirmpassword" id="confirmpassword" placeholder="Confirm Password">
                                        </div>

                                        <!-- <div class="form-group">
                                            <div class="checkbox checkbox-primary pr-10 pull-left">
                                                <input id="checkbox_2" required="" type="checkbox">
                                                <label for="checkbox_2"> Keep me logged in</label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div> -->
                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-info btn-rounded" onClick="validatePassword()">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="{{url('files/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{url('files/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{url('files/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{url('files/dist/js/jquery.slimscroll.js')}}"></script>

<!-- Init JavaScript -->
<script src="{{url('files/dist/js/init.js')}}"></script>
<script type="text/javascript" src="{{url('http://www.technicalkeeda.com/js/javascripts/plugin/jquery.js')}}"></script>
<script type="text/javascript" src="{{url('http://www.technicalkeeda.com/js/javascripts/plugin/jquery.validate.js')}}"></script>
<script>
    function validatePassword() {
        var validator = $("#loginForm").validate({
            rules: {
                password: "required",
                confirmpassword: {
                    equalTo: "#password"
                }
            },
            messages: {
                password: " Enter Password",
                confirmpassword: "<p style='color: red'>Confirm Password should be same as Password</p>"
            }
        });
        if (validator.form()) {
            $('#loginForm').submit();
//            alert('Success');
        }
    }

</script>
</body>

</html>