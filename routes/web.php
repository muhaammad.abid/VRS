<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middlewareGroups' => 'web'], function () {

    // For AuthController
    Route::get('logout', 'Auth\LoginController@logout');
    Route::group(['middleware' => 'unAuthenticated'], function()
    {
        Route::get('/', function () {
            return view('front.index');
        });


       Route::get('/form', function () {
           return view('front.form');
       });
       // Route::get('/form/', 'IndexController@form');
        Route::get('step1', 'IndexController@check');
        Route::get('model/{id}', 'IndexController@get_models');
        Route::get('year/{id}', 'IndexController@get_years');
        Route::get('all/{id}', 'IndexController@all');
        Route::get('form_data', 'IndexController@form_data');
        Route::get('importExport', 'Auth\LoginController@importExport');


        Route::get('check', [ 'as' => 'check', 'uses' => 'Auth\LoginController@check']);
        Route::get('login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@login_page']);

        Route::get('forgetpassword', 'Auth\LoginController@forget_page');
        Route::post('authuser', 'Auth\LoginController@authenticateUser');
        Route::post('forget/submit', 'Auth\LoginController@forget_submit');

    });

    // admin area
    Route::group(['middleware' => 'adminAuth'], function() {

        Route::get('vehicle_add', 'VehicleController@vehicle_add');
        Route::post('vehicle/submit', 'VehicleController@vehicle_submit');
        Route::get('admin/vehicles/delete/all', 'VehicleController@delete_all');
        Route::get('admin/dashboard', 'AdminController@dashboard');
        Route::get('changepassword', 'AdminController@changepassword');
        Route::post('savepassword', 'AdminController@savepassword');

        Route::get('admin/model/{id}', 'AdminController@model');
        Route::get('admin/year/{id}', 'AdminController@year');
        Route::get('admin/all/{id}', 'AdminController@all');
        Route::get('admin/id/{id}', 'AdminController@returnID');

        Route::post('importExcel', 'ImportController@importExcel');
        Route::get('downloadExcel/{type}', 'ExportController@downloadExcel');
        Route::get('admin/results', 'AdminController@results');
        Route::get('admin/result/delte/{id}', 'AdminController@single_delete');
        Route::get('admin/results/delete/all', 'AdminController@delete_all');

        Route::get('admin/result/edit/{id}', 'AdminController@edit_page');
        Route::get('form_data/update', 'UpdateController@updateResult');

        // result export 
        Route::get('admin/results/export', 'AdminController@exportResult');

        // product settings
        Route::get('admin/product/settings', 'SettingController@settingPage');
        Route::get('admin/product/batten/add', 'SettingController@battenAdd');
        Route::get('admin/product/batten/edit/{id}', 'SettingController@battenEdit');
        Route::get('admin/product/batten/delete/{id}', 'SettingController@battenDelete');
        Route::post('admin/product/batten/submit', 'SettingController@battenSubmit');
        Route::post('admin/product/batten/update', 'SettingController@battenUpdate');

        Route::get('admin/product/enduse/list', 'EnduseController@endusePage');
        Route::get('admin/product/enduse/add', 'EnduseController@enduseAdd');
        Route::get('admin/product/enduse/edit/{id}', 'EnduseController@enduseEdit');
        Route::get('admin/product/enduse/delete/{id}', 'EnduseController@enduseDelete');
        Route::post('admin/product/enduse/submit', 'EnduseController@enduseSubmit');
        Route::post('admin/product/enduse/update', 'EnduseController@enduseUpdate');

        Route::get('admin/product/chevron/list', 'ChevronController@chevronPage');
        Route::get('admin/product/chevron/add', 'ChevronController@chevronAdd');
        Route::get('admin/product/chevron/edit/{id}', 'ChevronController@chevronEdit');
        Route::get('admin/product/chevron/delete/{id}', 'ChevronController@chevronDelete');
        Route::post('admin/product/chevron/submit', 'ChevronController@chevronSubmit');
        Route::post('admin/product/chevron/update', 'ChevronController@chevronUpdate');

        Route::get('admin/product/escort/list', 'EscortController@escortPage');
        Route::get('admin/product/escort/add', 'EscortController@escortAdd');
        Route::get('admin/product/escort/edit/{id}', 'EscortController@escortEdit');
        Route::get('admin/product/escort/delete/{id}', 'EscortController@escortDelete');
        Route::post('admin/product/escort/submit', 'EscortController@escortSubmit');
        Route::post('admin/product/escort/update', 'EscortController@escortUpdate');

        Route::get('admin/product/bonnet/list', 'BonnetController@bonnetPage');
        Route::get('admin/product/bonnet/add', 'BonnetController@bonnetAdd');
        Route::get('admin/product/bonnet/edit/{id}', 'BonnetController@bonnetEdit');
        Route::get('admin/product/bonnet/delete/{id}', 'BonnetController@bonnetDelete');
        Route::post('admin/product/bonnet/submit', 'BonnetController@bonnetSubmit');
        Route::post('admin/product/bonnet/update', 'BonnetController@bonnetUpdate');

        Route::get('admin/product/template/list', 'TemplateController@templatePage');
        Route::get('admin/product/template/add', 'TemplateController@templateAdd');
        Route::get('admin/product/template/edit/{id}', 'TemplateController@templateEdit');
        Route::get('admin/product/template/delete/{id}', 'TemplateController@templateDelete');
        Route::post('admin/product/template/submit', 'TemplateController@templateSubmit');
        Route::post('admin/product/template/update', 'TemplateController@templateUpdate');

        Route::get('admin/product/grade/list', 'GradeController@gradePage');
        Route::get('admin/product/grade/add', 'GradeController@gradeAdd');
        Route::get('admin/product/grade/edit/{id}', 'GradeController@gradeEdit');
        Route::get('admin/product/grade/delete/{id}', 'GradeController@gradeDelete');
        Route::post('admin/product/grade/submit', 'GradeController@gradeSubmit');
        Route::post('admin/product/grade/update', 'GradeController@gradeUpdate');

        Route::get('admin/product/material/list', 'MaterialController@materialPage');
        Route::get('admin/product/material/add', 'MaterialController@materialAdd');
        Route::get('admin/product/material/edit/{id}', 'MaterialController@materialEdit');
        Route::get('admin/product/material/delete/{id}', 'MaterialController@materialDelete');
        Route::post('admin/product/material/submit', 'MaterialController@materialSubmit');
        Route::post('admin/product/material/update', 'MaterialController@materialUpdate');

        Route::get('admin/product/other/list', 'OtherController@otherPage');
        Route::get('admin/product/other/add', 'OtherController@otherAdd');
        Route::get('admin/product/other/edit/{id}', 'OtherController@otherEdit');
        Route::get('admin/product/other/delete/{id}', 'OtherController@otherDelete');
        Route::post('admin/product/other/submit', 'OtherController@otherSubmit');
        Route::post('admin/product/other/update', 'OtherController@otherUpdate');

    });


    // user area
    Route::group(['middleware' => 'user'], function() {

    });

});

